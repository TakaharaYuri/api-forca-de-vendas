'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Lead extends Model {
    contato() {
        return this.hasOne('App/Models/marketing/Contato', 'contato_id', 'id');
    }

    vendedor() {
        return this.hasOne('App/Models/gerencial/UsuarioDado', 'user_id', 'user_id');
    }

    produto() {
        return this.hasOne('App/Models/estoque/Produto', 'produto_id', 'id');
    }

    etiquetas() {
        return this.belongsToMany('App/Models/marketing/Etiqueta')
                   .pivotTable('etiquetas_leads')
    }
}

module.exports = Lead
