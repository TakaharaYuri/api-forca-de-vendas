'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PaginaContato extends Model {
    static get table() {
        return 'pagina_contato';
    }
}

module.exports = PaginaContato
