'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ModelosMockupInstagram extends Model {
  static get table() {
    return 'modelos_mockup_instagram';
  }
}

module.exports = ModelosMockupInstagram
