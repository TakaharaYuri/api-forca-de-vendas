'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Categoria extends Model {
    produtos() {
        return this.hasMany("App/Models/estoque/Produto", 'id', 'categoria_id');
      }
}

module.exports = Categoria
