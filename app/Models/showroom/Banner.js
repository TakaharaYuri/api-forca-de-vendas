'use strict'
const moment = require("moment");


/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Banner extends Model {
  static get dates() {
    return super.dates.concat(['data_inicio','data_fim'])
  }

  static castDates(field, value) {
    if (field === 'data_inicio' || field === 'data_fim') {
      return moment(value, "YYYY-MM-DD").format("YYYY-MM-DD");
    }
    return super.formatDates(field, value)
  }

}

module.exports = Banner
