'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const EmpresaModel = use('App/Models/gerencial/Empresa');
const ConfiguracaoModel = use('App/Models/gerencial/Configuracao');
const ProdutoModel = use('App/Models/estoque/Produto');
const UserModel = use('App/Models/User');
const ContatoModel = use('App/Models/marketing/Contato');
const LeadModel = use('App/Models/marketing/Lead');
const CategoriaModel = use('App/Models/estoque/Categoria');
const Encryption = use('Encryption');
const FormularioModel = use('App/Models/marketing/Formulario');
const FormularioItem = use('App/Models/marketing/FormularioItem');
const LeadFormularioItem = use('App/Models/marketing/LeadsFormulariosItem');
const GlobalService = use('App/Services/Global');
// const Env = use('Env');
// const dateFormat = use("dateformat");
// const moment = require('moment');




class IntegracaoController {
    async redirectByQRCode({ request, response, params }) {
        const empresa = await EmpresaModel.findOrFail(params.id);
        const configuracao = await ConfiguracaoModel.first();
        response.redirect(`${configuracao.main_url}/loja/${empresa.identificador}`);
        // return empresa;
    }

    async novoLead({ request, response, params }) {
        const headers = request.headers();
        const { key, user_id, produto, data_nascimento } = request.post();
        const empresa_id = Encryption.decrypt(key);
        var contato = request.only(['nome', 'email', 'celular', 'email', 'cpf']);
        var lead = request.only([
            'metodo_pagamento',
            'valor_entrada',
            'mensagem',
            'origem',
            'tipo',
            'intencao_retirada',
            'valor_lance',
            'valor_parcelas',
            'prazo_ideal'
        ]);
        var meta_dados = request.only([
            'possui_carro',
            'possui_moto',
        ]);

        lead.raw = JSON.stringify(request.post());
        lead.headers = JSON.stringify(headers);
        // contato.data_nascimento = dateFormat(contato.data_nascimento, "yyyy-mm-dd");
        // contato.cpf = (contato.cpf)? contato.cpf.replace(/[^\w\s]/gi, '').replace(/\s/g, ''):null;
        // contato.celular = (contato.celular)? contato.celular.replace(/[^\w\s]/gi, '').replace(/\s/g, ''):null;

        const empresa = await EmpresaModel.find(empresa_id);

        if (empresa) {

            let vendedor = await UserModel.query().where('id', user_id).with('usuario_dados').first();
            vendedor.dados = await vendedor.usuario_dados().first();

            if (vendedor) {

                const selectedProduct = await ProdutoModel.findBy('id', produto);

                contato.empresa_id = empresa.id
                // contato.data_nascimento = moment(contato.data_nascimento).format('YYYY-MM-DD');
                const queryContato = await ContatoModel.create(contato);

                lead.contato_id = queryContato.id;
                lead.user_id = vendedor.id;
                lead.produto_id = selectedProduct.id;
                lead.empresa_id = empresa.id;

                const queryLead = await LeadModel.create(lead);
                const entradaFormatada = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(queryLead.valor_entrada);
                const valorLanceFormatada = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(queryLead.valor_lance);

                if (queryLead) {
                    var _msg = `Olá ${vendedor.dados.nome} eu sou o *${queryContato.nome}*, acabei de preencher o formulário na página da ${empresa.nome} e gostaria de fazer uma simulação com os seguintes dados:\n`;
                    if (selectedProduct) _msg = `${_msg}\n *Modelo de Interesse*: ${selectedProduct.nome}`;
                    if (contato.cpf) _msg = `${_msg}\n *CPF*: ${contato.cpf}`;
                    if (data_nascimento) _msg = `${_msg}\n *Data Nascimento*: ${data_nascimento}`;
                    if (meta_dados.possui_carro) _msg = `${_msg}\n *Possui Carro*: ${meta_dados.possui_carro}`;
                    if (meta_dados.possui_moto) _msg = `${_msg}\n *Possui Moto*: ${meta_dados.possui_moto}`;
                    if (lead.metodo_pagamento) _msg = `${_msg}\n *Metódo de Pagamento*: ${lead.metodo_pagamento}`;
                    if (lead.intencao_retirada) _msg = `${_msg}\n *Intenção de Retirada*: ${lead.intencao_retirada}`;
                    if (lead.valor_lance) _msg = `${_msg}\n *Valor de Lance*: ${valorLanceFormatada}`;
                    if (queryLead.valor_entrada) _msg = `${_msg}\n *Entrada de:*: ${entradaFormatada}`;
                    if (lead.valor_parcelas) _msg = `${_msg}\n *Parcelas que cabem no orçamento*: ${lead.valor_parcelas}`;
                    if (lead.prazo_ideal) _msg = `${_msg}\n *Prazo Ideal*: ${lead.prazo_ideal}`;

                    var link = `https://api.whatsapp.com/send?phone=${vendedor.dados.celular}&text=${encodeURIComponent(_msg)}`;

                    return {
                        result: true,
                        whatsapp_link: link
                    };
                }
                else {
                    response.status(400).send({
                        result: false,
                        msg: 'Ocorreu um problema ao processar os dados do Lead'
                    });
                }

            }
            else {
                response.status(400).send({
                    result: false,
                    msg: 'Vendedor não encontrado'
                });
            }
        }
        else {
            response.status(400).send({
                result: false,
                msg: 'A empresa informada não é valida'
            });
        }
    }

    async novoLeadDinamico({ request, response }) {
        const headers = request.headers();
        const rawData = request.all();
        let formCustomData = [];
        let selectedProduct;
        const { key, configuracoes, user_id } = request.all();
        const form = request.only('form');
        let contato = request.only(['form.nome', 'form.email', 'form.celular', 'form.email', 'form.cpf', 'form.data_nascimento', 'form.produto']);
        contato = contato.form;

        const empresa = await EmpresaModel.query().where('key', key).first();

        if (empresa) {
            let vendedor = await UserModel.query().where('id', user_id).with('usuario_dados').first();
            vendedor.dados = await vendedor.usuario_dados().first();

            if (rawData.form.produto) {
                selectedProduct = await ProdutoModel.findBy('id', rawData.form.produto);
            } 

            if (vendedor) {
                let objectKeysForm = Object.keys(rawData.form.custom);

                for (let index = 0; index < objectKeysForm.length; index++) {
                    let campoPersonalizado = await FormularioItem.find(Number(objectKeysForm[index]));
                    let valor = rawData.form.custom[objectKeysForm[index]];
                    let campo = campoPersonalizado.name;

                    if (campoPersonalizado.type == 'money_slide') {
                        const valorFormatado = GlobalService.moneyFormat(valor);
                        valor = valorFormatado;
                    }
                    const normalize = {
                        'campo': campo, 
                        'valor': valor
                    }

                    formCustomData.push(normalize);
                }


                var _msg = `Olá ${vendedor.dados.nome} eu sou o *${rawData.form.nome}*, acabei de preencher o formulário na página da ${empresa.nome} e gostaria de fazer uma simulação com os seguintes dados:\n\n`;
                if (rawData.form.nome) _msg = `${_msg}\n *Nome: *: ${rawData.form.nome}`;
                if (rawData.form.cpf) _msg = `${_msg}\n *CPF*: ${rawData.form.cpf}`;
                if (rawData.form.data_nascimento) _msg = `${_msg}\n *Data Nascimento*: ${rawData.form.data_nascimento}`;
                if (rawData.form.produto) _msg = `${_msg}\n *Modelo de Interesse: *: ${selectedProduct.nome}`;

                formCustomData.forEach(element => {
                    _msg = `${_msg}\n *${element.campo}*: ${element.valor}`;
                });

                var link = `https://api.whatsapp.com/send?phone=${GlobalService.cleanString(vendedor.dados.celular)}&text=${encodeURIComponent(_msg)}`;
                return {
                    result: true,
                    whatsapp_link: link
                };

                
            }
            else {
                response.status(400).send({
                    result: false,
                    msg: 'Ocorreu um problema ao processar os dados do Lead'
                });
            }
        }
        else {
            response.status(400).send({
                result: false,
                msg: 'A empresa informada não é valida'
            });
        }
        
        return form;
    }

    // async registraAcesso({ request, response, params }) {
    //     const { key } = params;
    //     const empresa_id = Encryption.decrypt(key);

    //     const empresa = await EmpresaModel.query().where('id', empresa_id).setHidden(['id']).first();
    //     const _formulario = await FormularioModel.query()
    //         .where('empresa_id', empresa_id)
    //         .with('items', (builder) => {
    //             builder.setHidden(['id', 'created_at', 'updated_at', 'raw_data', 'position_index', 'formulario_id']);
    //         })
    //         .setHidden(['id', 'status', 'empresa_id', 'created_at', 'updated_at'])
    //         .first();

    //     if (_formulario) {
    //         const formulariosJson = _formulario.toJSON();
    //         formulariosJson.items.map(item => {
    //             item.options = JSON.parse(item.options);
    //             item.conditions = JSON.parse(item.conditions);
    //             item.selected_options = JSON.parse(item.selected_options);
    //         });

    //         empresa.formulario = formulariosJson;
    //     }

    //     const categoria_produtos = await CategoriaModel.query().with('produtos').where('empresa_id', empresa.id).fetch();
    //     empresa.categoria_produtos = categoria_produtos;
    //     return empresa;
    // }

    async configuracoes({ request, response }) {
        const { key } = request.post();
        const empresa_id = Encryption.decrypt(key);
        const empresa = await EmpresaModel.query().where('id', empresa_id)
            .setHidden(['id'])
            .first();

        const _formulario = await FormularioModel.query()
            .where('empresa_id', empresa.id)
            .with('items', (builder) => {
                builder.orderBy('position_index', 'ASC')
                    .where('enabled', 1)
                    .setHidden(['created_at', 'updated_at', 'raw_data', 'formulario_id']);
            })
            .setHidden(['id', 'status', 'empresa_id', 'created_at', 'updated_at'])
            .first();

        if (_formulario) {
            const formulariosJson = _formulario.toJSON();
            formulariosJson.items.map(item => {
                item.options = JSON.parse(item.options);
                item.conditions = JSON.parse(item.conditions);
                item.selected_options = JSON.parse(item.selected_options);
            });

            empresa.formulario = formulariosJson;
        }
        const categoria_produtos = await CategoriaModel.query().with('produtos').where('empresa_id', empresa.id).fetch();
        empresa.categoria_produtos = categoria_produtos;
        return empresa;
    }
}

module.exports = IntegracaoController
