'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with categorias
 */

const CategoriaModel = use('App/Models/estoque/Categoria');

class CategoriaController {
  /**
   * Show a list of all categorias.
   * GET categorias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { client_id } = request.all();
    const data = await CategoriaModel.query().where('empresa_id', client_id).fetch();
    return data;
  }


  /**
   * Create/save a new categoria.
   * POST categorias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.post();
    const { client_id } = request.all();
    data.empresa_id = client_id;

    const query = await CategoriaModel.create(data);
    return query;
  }



  /**
   * Update categoria details.
   * PUT or PATCH categorias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const categoria = await CategoriaModel.findOrFail(params.id);
    const data = request.post();
    categoria.merge(data);
    await categoria.save();

    return categoria;
  }

  /**
   * Delete a categoria with id.
   * DELETE categorias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const categoria = await CategoriaModel.findOrFail(params.id);
    return await categoria.delete();
  }
}

module.exports = CategoriaController
