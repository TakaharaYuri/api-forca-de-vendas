"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with produtos
 */

const ProdutoModel = use("App/Models/estoque/Produto");

class ProdutoController {
  /**
   * Show a list of all produtos.
   * GET produtos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();
    const produto = await ProdutoModel.query().with('categoria').where('empresa_id',client_id).fetch();

    return produto;
  }

  /**
   * Create/save a new produto.
   * POST produtos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { client_id } = request.all();
    const data = request.post();
    data.empresa_id = client_id;
    console.log('Produto ->', data);

    const query = await ProdutoModel.create(data);
    return query;
  }

  /**
   * Update produto details.
   * PUT or PATCH produtos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only(['nome', 'categoria_id','imagem','ativo', 'preco_venda','tipo']);
    const produto = await ProdutoModel.findOrFail(params.id);

    produto.merge(data);
    await produto.save();
    return produto;
  }

  /**
   * Delete a produto with id.
   * DELETE produtos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const produto = await ProdutoModel.findOrFail(params.id);
    await produto.delete();
  }
}

module.exports = ProdutoController;
