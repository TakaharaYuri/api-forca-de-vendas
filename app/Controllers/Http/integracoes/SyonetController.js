'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with syonets
 */

const soap = use('soap');
const syonetService = use('App/Services/SyonetIntegration');

class SyonetController {
  /**
   * Show a list of all syonets.
   * GET syonets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async gerar_evento({ request, response, view }) {

    const dados = request.post();
    // const dados = {
    //   'usuario': 'site',
    //   'senha': 'site',
    //   'dominio': 'LINHARES',
    //   'idEmpresa': '1',
    //   'grupoEvento': 'OPORTUNIDADE',
    //   'tipoEvento': 'NOVOS WEB',
    //   'idUsuario': '0',
    //   'origem': 'LANDING PAGE',
    //   'idCliente': '0',
    //   'cpfCnpj': '0',
    //   'idEvento': '0',
    //   'status': '',
    //   'classificacao': '',
    //   'prioridade': '',
    //   'observacao': 'LEAD DE TESTE',
    //   'midia': 'LP - TESTE',
    //   'nome': 'Nome teste',
    //   'email': 'email@teste3333@gmail.com',
    //   'telefoneCelular': '(85) 9 99917739',
    //   'assunto': 'LEAD TESTE',
    // }

    let telefone = dados.telefoneCelular;
   
    if (telefone.includes('(')) {
      telefone = telefone.split(new RegExp(/^\((\d{2})\)/));
      dados.dddCelular = telefone[1];
      dados.telefoneCelular = telefone[2].replace(/[^A-Z0-9]+/ig, '');
    }
    else {
       telefone = telefone.split('').slice(0, 1).join('');
      let numero = telefone.split('');
      dados.dddCelular = numero.slice(0, 2).join('');
      dados.telefoneCelular = numero.slice(2, 10).join('');
    }

    // return dados;
    // return dados;
    const { loja } = request.get();
    const options = [];

    options['terra_santa'] = {
      'usuario': 'site',
      'senha': 'site1991',
      'dominio': 'LINHARES',
      'senhaSite': 'site',
      'idEmpresa': dados.idEmpresa, 
      'origemPadrao': 'DIRETO'
    }

    options['renault'] = {
      'usuario': 'site',
      'senha': 'site1991',
      'dominio': 'LINHARES',
      'senhaSite': 'site',
      'idEmpresa': dados.idEmpresa,
      'origemPadrao': 'DIRETO'
    }

    options['honda'] = {
      'usuario': 'site',
      'senha': 'site1991',
      'dominio': 'LINHARES',
      'senhaSite': 'site',
      'idEmpresa': dados.idEmpresa,
      'origemPadrao': 'DIRETO'
    }

    options['honda'] = {
      'usuario': 'site',
      'senha': 'site1991',
      'dominio': 'LINHARES',
      'senhaSite': 'site',
      'idEmpresa': dados.idEmpresa,
      'origemPadrao': 'DIRETO'
    }

    options['midia9'] = {
      'usuario': 'SITE_MIDIA9',
      'senha': 'Midia9!2021',
      'dominio': 'ADTSA',
      'senhaSite': 'Midia9!2021',
      'idEmpresa': dados.idEmpresa,
      'origemPadrao': 'ORGÂNICO'
    }

    try {
      const syonet = await syonetService.gerarEvento(dados, options[loja]);
      return {
        result: true,
        data: syonet
      }
    } catch (error) {
      return {
        result: false,
        error: error
      }
    }
  }

  async gerar_evento_wordPress({ request, response, view }) {
    const dados = request.post();
    const { loja } = request.get();

    try {

      let options = [];
      let observacao = '';


      if (dados.fields.linhares) {

        if (dados.fields.modelo) {
          observacao += `Modelo de Interesse: ${dados.fields.modelo.value}\n`;
        }

        if (dados.fields.cpf) {
          observacao += `CPF: ${dados.fields.cpf.value}\n`
        }

        if (dados.fields.usado_entrada) {
          observacao += `Usado na Entrada: ${dados.fields.usado_entrada.value}\n`;
        }

        if (dados.fields.valor_disponivel) {
          observacao += `Valor Disponível: ${dados.fields.valor_disponivel.value}\n`;
        }

        if (dados.fields.valor_disponivel) {
          observacao += `Data Pretenção: ${dados.fields.data_pretencao.value}\n`;
        }

        if (dados.fields.observacao) {
          observacao += `Observação: ${dados.fields.observacao.value}`;
        }
      }

      options['terra_santa'] = {
        'usuario': 'site',
        'senha': 'site1991',
        'dominio': 'LINHARES',
        'senhaSite': 'site',
        'idEmpresa': dados.fields.idEmpresa.value,
        'origemPadrao': 'DIRETO'
      }

      options['midia9'] = {
        'usuario': 'SITE_MIDIA9',
        'senha': 'Midia9!2021',
        'dominio': 'ADTSA',
        'senhaSite': 'Midia9!2021',
        'idEmpresa': dados.fields.idEmpresa.value,
        'origemPadrao': 'ORGÂNICO'
      }

      let normalization = {
        usuario: options[loja].usuario,
        senha: options[loja].senhaSite,
        dominio: options[loja].dominio,
        idEmpresa: dados.fields.idEmpresa.value,
        grupoEvento: dados.fields.grupoEvento.value,
        tipoEvento: dados.fields.tipoEvento.value,
        origem: dados.fields.origem.value,
        idUsuario: '0',
        idCliente: '0',
        cpfCnpj: '0',
        idEvento: '0',
        // status: '',
        // classificacao: '',
        // prioridade: '',
        observacao: observacao,
        midia: dados.fields.midia.value || options[loja].origemPadrao,
        nome: dados.fields.nome.value,
        email: dados.fields.email.value,
        telefoneCelular: dados.fields.telefoneCelular.value,
        assunto: dados.fields.assunto.value,
        url: (dados.fields.url) ? dados.fields.url.value : null
      }



      let telefone = normalization.telefoneCelular;

      if (telefone.includes('(')) {
        telefone = telefone.split(new RegExp(/^\((\d{2})\)/));
        normalization.dddCelular = telefone[1];
        normalization.telefoneCelular = telefone[2].replace(/[^A-Z0-9]+/ig, '');
      }
      else {
        normalization.dddCelular = telefone.substring(0,2);
        normalization.telefoneCelular = telefone.substring(2);
      }


      try {
        const syonet = await syonetService.gerarEvento(normalization, options[loja]);
        return {
          result: true,
          data: syonet
        }
      } catch (error) {
        return {
          result: false,
          error: error
        }
      }

    } catch (error) {
      console.log(error);
    }

  }

  /**
   * Render a form to be used for creating a new syonet.
   * GET syonets/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new syonet.
   * POST syonets
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single syonet.
   * GET syonets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing syonet.
   * GET syonets/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update syonet details.
   * PUT or PATCH syonets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a syonet with id.
   * DELETE syonets/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = SyonetController
