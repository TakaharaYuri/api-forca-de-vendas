'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const sinespService = use('sinesp-api');
const fipeService = use('App/Services/Fipe');
const MarcaModeloModel = use('App/Models/gerencial/MarcasModelos');

/**
 * Resourceful controller for interacting with fipes
 */
class FipeController {
  /**
   * Show a list of all fipes.
   * GET fipes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
  * Show a list of all fipes.
  * GET fipes
  *
  * @param {object} ctx
  * @param {Request} ctx.request
  * @param {Response} ctx.response
  * @param {View} ctx.view
  */
  async consultarPlaca({ request, response, params }) {
    let veiculo = {};
    let fipeData = {};
    const dadosSinesp = await sinespService.search(params.placa);
    const tabelasFipe = await fipeService.request('ConsultarTabelaDeReferencia');


    veiculo = {
      tipo_veiculo_fipe: 1,
      codigo_table_fipe: tabelasFipe[0].Codigo,
      ano: dadosSinesp.ano,
      ano_modelo: dadosSinesp.anoModelo,
      chassi: dadosSinesp.chassi,
      cor: dadosSinesp.cor,
      marca: dadosSinesp.marca.split('/')[0],
      modelo: dadosSinesp.modelo,
      municipio: dadosSinesp.municipio,
      placa: dadosSinesp.placa,
      situacao: dadosSinesp.situacao,
      uf: dadosSinesp.uf
    }

    fipeData = {
      codigoTipoVeiculo: 1,
      codigoTabelaReferencia: tabelasFipe[0].Codigo,
    }

    let marcas =  await fipeService.request('ConsultarMarcas', fipeData);

    let marca = marcas.find(item => {
      let _marca = item.Label.toUpperCase();
      if (_marca.includes(veiculo.marca)) {
        return _marca;
      }
    });

    // let marca = await MarcaModeloModel.query().where('nome', veiculo.modelo.split(' ')).first();



    console.log('veiculo', veiculo);
    console.log('marca', marca);


    


    veiculo.marca_id = marca.Value;

    fipeData.codigoMarca = veiculo.marca_id;
    fipeData.tipoConsulta = 'Tradicional';
    fipeData.anoModelo = veiculo.ano_modelo;
    fipeData.modeloCodigoExterno = "";
    fipeData.codigoModelo = "";

    const anosModelo = await fipeService.request('ConsultarModelosAtravesDoAno', fipeData);
    console.log('anosModelo ->', fipeData);
    let modeloNominal;
    

    if (veiculo.modelo.includes('/')) {
      modeloNominal = veiculo.modelo.split('/')[1].toUpperCase();
    }
    
    else if (veiculo.modelo.includes(' ')) {
      modeloNominal = veiculo.modelo.split(' ')[0].toUpperCase();
    }

    else if (veiculo.modelo.includes('-')) {
      modeloNominal = veiculo.modelo.split('-')[1].toUpperCase();
    }

    // console.log(veiculo.modelo);
    // console.log(modeloNominal);
    // console.log(anosModelo);
    

    const versoesModelo = anosModelo.filter(item => item.Label.toUpperCase().localeCompare(veiculo.modelo.toUpperCase()));
    console.log('versoesModelo', versoesModelo);
    // veiculo.modelos = versoesModelo;

    fipeData.codigoModelo = versoesModelo.find( item => item.Label.toUpperCase().localeCompare(veiculo.modelo.toUpperCase()));

    console.log('codigoModelo', fipeData.codigoModelo);

    const versoesCombustivel = await fipeService.request('ConsultarAnoModelo', fipeData);

    const versaoModeloCombustivel = versoesCombustivel.find(item => item.Value.includes(veiculo.ano_modelo));

    veiculo.combustivel = versaoModeloCombustivel.Label.split(' ')[1];

    fipeData.codigoTipoCombustivel = versaoModeloCombustivel.Value.split('-')[1];

    const detalhesModelo = await fipeService.request('ConsultarValorComTodosParametros', fipeData);

    console.log('detalhesModelo', detalhesModelo);

    veiculo.modelo = detalhesModelo.Modelo;
    veiculo.valor = detalhesModelo.Valor;
    veiculo.tipo_veiculo = detalhesModelo.TipoVeiculo;
    veiculo.ano_modelo = detalhesModelo.AnoModelo;
    veiculo.codigo_fipe = detalhesModelo.CodigoFipe;


    if (veiculo.modelo.includes('5p')) { veiculo.portas = 5; }
    if (veiculo.modelo.includes('4p')) { veiculo.portas = 4; }
    if (veiculo.modelo.includes('3p')) { veiculo.portas = 3; }

    veiculo.flex = (veiculo.modelo.localeCompare('flex'))? true:false;

    if (veiculo.modelo.localeCompare('Mec', 'pt-BR')) { veiculo.cambio = 'Mecanico' }
    if (veiculo.modelo.localeCompare('Aut', 'pt-BR')) { veiculo.cambio = 'Automatico' }

    if (veiculo.modelo.includes('1.0')) { veiculo.motorizacao = '1.0' };
    if (veiculo.modelo.includes('1.2')) { veiculo.motorizacao = '1.2' };
    if (veiculo.modelo.includes('1.3')) { veiculo.motorizacao = '1.3' };
    if (veiculo.modelo.includes('1.4')) { veiculo.motorizacao = '1.4' };
    if (veiculo.modelo.includes('1.5')) { veiculo.motorizacao = '1.5' };
    if (veiculo.modelo.includes('1.6')) { veiculo.motorizacao = '1.6' };
    if (veiculo.modelo.includes('1.8')) { veiculo.motorizacao = '1.8' };
    if (veiculo.modelo.includes('3.0')) { veiculo.motorizacao = '3.0' };



    return response.json({
      // tabelas: tabelasFipe,
      sinesp: dadosSinesp,
      veiculo: veiculo,
      detalhesModelo: detalhesModelo
    })

    // try {
    //   let sinespData = await sinespApi.search(params.placa);
    //   return response.json({
    //     result: true,
    //     data: sinespData
    //   })
    // } catch (error) {

    //   return response.status(500).json({
    //     result: false,
    //     message: error.message
    //   })
    // }
  }


  /**
   * Create/save a new fipe.
   * POST fipes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

}

module.exports = FipeController
