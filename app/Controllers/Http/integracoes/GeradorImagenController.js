'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with geradorimagens
 */

const { createCanvas, loadImage, Image } = require('canvas');
const modelMockupInstagram = use('App/Models/ModelosMockupInstagram');

class GeradorImagenController {
  /**
   * Show a list of all geradorimagens.
   * GET geradorimagens
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async index({ request, response }) {
    let query = await modelMockupInstagram.query().where('status', true).orderBy('id', 'DESC').fetch();
    return query;
  }

  async generate({ request, response }) {

    let requestData = request.post();
    const modelData = await modelMockupInstagram.find(requestData.id);
    const configurations = JSON.parse(modelData.configuracoes);
    const width = 1080;
    const height = 1080;
    const canvas = createCanvas(width, height)
    const context = canvas.getContext('2d');
    let cropImageArray = requestData.imagem.split('upload/');
    const cropImage = `${cropImageArray[0]}upload/w_350,c_fill,ar_1:1,g_auto,r_max/${cropImageArray[1]}`;

    try {
      let profile = await loadImage(cropImage);
      context.drawImage(profile, configurations.cordinates.profile.axys_y,
                                 configurations.cordinates.profile.axys_x,
                                 configurations.cordinates.profile.width,
                                 configurations.cordinates.profile.height);

      try {

        let background = await loadImage(modelData.background);
        context.drawImage(background, configurations.cordinates.background.axys_y,
                                      configurations.cordinates.background.axys_x,
                                      configurations.cordinates.background.width,
                                      configurations.cordinates.background.height);

        context.fillStyle = configurations.cordinates.name.color;
        context.font = configurations.cordinates.name.font;
        context.textBaseline = "middle";
        context.textAlign = "center";
        context.fillText(requestData.nome, configurations.cordinates.name.axys_x,
                                            configurations.cordinates.name.axys_y,
                                            configurations.cordinates.name.width);

        context.fillStyle = configurations.cordinates.whatsapp.color;
        context.font = configurations.cordinates.whatsapp.font;
        context.fillText(requestData.celular, configurations.cordinates.whatsapp.axys_x,
          configurations.cordinates.whatsapp.axys_y);
      }
      catch (error) {
        return {
          result: false,
          error: error.message,
          step:2
        }
      }

      const data = canvas.toDataURL('image/png');
      return {
        result: true,
        data: data
      }
    }
    catch (error) {
      return {
        result: false,
        error: error.message,
        step:1
      }
    }





    // // Desenhando o Blackground
    // let background = await loadImage('app/public/assets/images/base.png');
    // context.drawImage(background, 150, 65, 327, 327);
    // context.globalCompositeOperation = 'destination-in';

    // //Desenhando Imagem de Perfil
    // let background = await loadImage('app/public/assets/images/base.png');
    // context.drawImage(background, 150, 65, 327, 327);
    // context.globalCompositeOperation = 'destination-in';

  }

}

module.exports = GeradorImagenController
