"use strict";

const userModel = use("App/Models/User");
const usuarioDadosModel = use("App/Models/gerencial/UsuarioDado");
const usuarioEmpresaModel = use("App/Models/gerencial/UsuarioEmpresa");
const EmpresaModel = use('App/Models/gerencial/Empresa');
const permissaoModel = use('App/Models/gerencial/Permissoe');
const moduloModel = use('App/Models/gerencial/Modulo');
const db = use("Database");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/auth/src/Schemes/Session')} AuthSession */
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

class AuthController {
  async register({ request }) {
    const userData = request.only(["username", "email", "password"]);
    let usuarioDadosComplementares = request.only([
      "nome",
      "celular",
      "imagem",
    ]);

    const user = await userModel.create(userData);

    usuarioDadosComplementares.user_id = user.id;
    const usuarioDados = await usuarioDadosModel.create(
      usuarioDadosComplementares
    );

    return {
      usuario: user,
      complementar: usuarioDados,
    };
  }

  /**
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   */

  async login({ request, auth }) {
    let data = {};
    let userData;
    const { email, password } = request.all();
    if (await auth.attempt(email, password)) {
      const user = await userModel.query()
                                  .where("email", email)
                                  .with('usuario_dados', (builder) => {
                                    builder.setHidden(['id']);
                                  }).first();

      if (user.status == 1) {
        const token = await auth.generate(user);
        // const complementaryData = await usuarioDadosModel.findBy("user_id",user.id);

        userData = user.toJSON();
        data = userData;
        Object.assign(data, userData.usuario_dados);

        data.token = token;
        data.result = true;

        const empresas = await db.select('empresas.*','setor_id')
                                 .from('empresas')
                                 .innerJoin('usuario_empresas','usuario_empresas.empresa_id','empresas.id')
                                 .where('usuario_empresas.user_id', userData.id);

        for (const empresa of empresas) {
          empresa.setor = await db
            .from("setores")
            .where("id", empresa.setor_id)
            .first();
        }

        data.empresa_padrao = empresas[0];
        data.empresas = empresas;

        let modulos = await permissaoModel.query()
                                          .select('modulos.*','id_modulo')
                                          .innerJoin('modulos', 'id_modulo', 'modulos.id')
                                          .where('id_setor', data.empresa_padrao.setor.id)
                                          .where('modulos.status', 1).whereNull('id_grupo')
                                          .orderBy('posicao')
                                          .fetch();
        modulos = modulos.toJSON();

        for (const item of modulos) {
          item.subs = await permissaoModel.query()
                                          .select('modulos.*','id_modulo')
                                          .innerJoin('modulos', 'id_modulo', 'modulos.id')
                                          .where('id_grupo', item.id)
                                          .where('id_setor', data.empresa_padrao.setor.id)
                                          .where('status', 1)
                                          .groupBy('id')
                                          .orderBy('posicao')
                                          .fetch();
                                      //  .where('status', 1)
                                      //  .orderBy('posicao')
                                      //  .fetch();
        }
        
        data.permissoes = modulos;
      }
      
      else if (user.status == 0) {
        data.result = false;
        data.msg = 'Conta bloqueada, entre em contato com o administrador do sistema'
      }
      else if (user.status == 2) {
        data.result = false;
        data.msg = 'Você ainda não ativou sua conta'
      }
    }

    return data;
  }
}

module.exports = AuthController;
