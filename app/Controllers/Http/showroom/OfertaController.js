'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with ofertas
 */
const moment = use('moment');
const model = use('App/Models/showroom/Oferta');

class OfertaController {
  /**
   * Show a list of all ofertas.
   * GET ofertas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response }) {
    const { client_id, page, nome = '', status = '', created_at = '' } = request.get();
    let query = model.query().with('modelo').where('empresa_id', client_id);

    if (nome != '') {
      query = query.whereRaw(`nome LIKE '%${nome}%'`);
    }
    if (status != '') {
      query = query.where('status', status);
    }
    if (created_at != '') {
      query = query.where('created_at', created_at);
    }

    query = await query.orderBy('created_at', 'DESC').paginate(page ,10);

    query = query.toJSON();

    for (const item of query.data) {
      item.vencido = (item.data_fim < moment().format('YYYY-MM-DD')) ? true : false;
      item.modelo.imagens = (item.modelo.imagens)? JSON.parse(item.modelo.imagens):null;
    }

    return response.json(query);
  }


  /**
   * Create/save a new oferta.
   * POST ofertas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    const { client_id } = request.all();
    if (data.meta_dados) {
      data.meta_dados = JSON.stringify(data.meta_dados);
      data.selos = JSON.stringify(data.selos);
      data.empresa_id = client_id;
      data.url = data.nome.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
    }

    const query = await model.create(data);
    return query;
  }

  /**
   * Display a single oferta.
   * GET ofertas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request }) {
    const { client_id } = request.all();
    let query = await model.query().where('empresa_id', client_id).where('id', params.id).first();
    if (query) {
      query.meta_dados = JSON.parse(query.meta_dados);
      query.selos = JSON.parse(query.selos);
    }

    return query;
  }


  /**
   * Update oferta details.
   * PUT or PATCH ofertas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const query = await model.findOrFail(params.id);
    if (!query.url) data.url = data.nome.replace(/[^A-Z0-9]+/ig, '-').toLowerCase();
    if (data.meta_dados) {
      data.meta_dados = JSON.stringify(data.meta_dados);
      data.selos = JSON.stringify(data.selos);
    }

    query.merge(data);
    await query.save();
    return query;
  }

  /**
   * Delete a oferta with id.
   * DELETE ofertas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { client_id } = request.all();
    const modelo = await model.query().where('id', params.id).where('empresa_id', client_id).first();
    return await modelo.delete();
  }
}

module.exports = OfertaController
