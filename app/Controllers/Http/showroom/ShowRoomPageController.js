'use strict'

const EmpresaModel = use("App/Models/gerencial/Empresa");
const OfertasModel = use('App/Models/showroom/Oferta');
const BannerModel = use('App/Models/showroom/Banner');
const PaginaShowroomModel = use('App/Models/showroom/PaginaShowroom');
const View = use('View');
const Antl = use('Antl');
const moment = use('moment');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with showroompages
 */
class ShowRoomPageController {
  /**
   * Show a list of all showroompages.
   * GET showroompages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    return request.all();
  }

  /**
   * Render a form to be used for creating a new showroompage.
   * GET showroompages/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async showroom({ params, view, request }) {
    const { host } = request.headers()
    let data = {};
    let empresa = await EmpresaModel.findBy("host", host);
    if (empresa) {
      let banners = await BannerModel.query()
        .where('empresa_id', empresa.id)
        .where('status', 1)
        .where('data_inicio', '<=', moment().format('YYYY-MM-DD'))
        .where('data_fim', '>', moment().format('YYYY-MM-DD'))
        .fetch();

      let layout = await PaginaShowroomModel.query().where('empresa_id', empresa.id).first();

      let ofertas = await OfertasModel.query().with('modelo').where('empresa_id', empresa.id)
        .where('status', 1)
        .where('data_inicio', '<=', moment().format('YYYY-MM-DD'))
        .where('data_fim', '>', moment().format('YYYY-MM-DD'))
        .fetch();

      ofertas = ofertas.toJSON();
      banners = banners.toJSON();

      if (layout) {
        layout.layout = (layout.layout) ? JSON.parse(layout.layout) : {};
        layout.seo = (layout.seo) ? JSON.parse(layout.seo) : {};
        layout.configuracoes = (layout.configuracoes) ? JSON.parse(layout.configuracoes) : {};
        layout.meta_dados = (layout.meta_dados) ? JSON.parse(layout.meta_dados) : {};
      }

      ofertas.map(item => {
        item.selos = (item.selos) ? JSON.parse(item.selos) : {};
        item.meta_dados = (item.meta_dados) ? JSON.parse(item.meta_dados) : {};
        item.modelo.imagens = (item.modelo.imagens) ? JSON.parse(item.modelo.imagens) : {};
        if (item.data_fim) item.finalizar_oferta = moment(item.data_fim, "YYYY-MM-DD").format("MMM DD, YYYY 00:00:00");
      });

      data = {
        empresa: empresa,
        ofertas: ofertas,
        banners: banners,
        tema: layout
      }

      View.global('currency', (value) => {
        return Antl.formatNumber(value, { style: 'currency', currency: 'brl' });
      });

      // return data;
      // return view.render("modelos/site/hyundai/home", { data: data });
      return { data: data }
    }
    else {
      return { data: false };
    }
  }

  async details({ params, view, request }) {
    const { host } = request.headers();
    let empresa = await EmpresaModel.findBy("host", host);
    let data = {};
    if (empresa) {
      let oferta = await OfertasModel.query()
        .with('modelo')
        .where('empresa_id', empresa.id)
        .where('status', 1)
        .where('url', params.url)
        .first();
      oferta = oferta.toJSON();
      let layout = await PaginaShowroomModel.query().where('empresa_id', empresa.id).first();

      let ofertas = await OfertasModel.query().with('modelo').where('empresa_id', empresa.id)
        .where('status', 1)
        .where('data_inicio', '<=', moment().format('YYYY-MM-DD'))
        .where('data_fim', '>', moment().format('YYYY-MM-DD'))
        .where('id', '<>', oferta.id)
        .fetch();

      ofertas = ofertas.toJSON();

      if (layout) {
        layout.layout = JSON.parse(layout.layout);
        layout.seo = JSON.parse(layout.seo);
        layout.configuracoes = JSON.parse(layout.configuracoes);
        layout.meta_dados = JSON.parse(layout.meta_dados);
      }

      ofertas.map(item => {
        item.selos = JSON.parse(item.selos);
        item.meta_dados = JSON.parse(item.meta_dados);
        item.modelo.imagens = JSON.parse(item.modelo.imagens);
        if (item.data_fim) item.finalizar_oferta = moment(item.data_fim, "YYYY-MM-DD").format("MMM DD, YYYY 00:00:00");
        // item.selected_options = JSON.parse(item.selected_options);
      });

      if (oferta) {
        oferta.selos = JSON.parse(oferta.selos);
        oferta.meta_dados = JSON.parse(oferta.meta_dados);
        oferta.modelo.imagens = JSON.parse(oferta.modelo.imagens);
        oferta.ofertas = ofertas;
        if (oferta.data_fim) oferta.finalizar_oferta = moment(oferta.data_fim, "YYYY-MM-DD").format("MMM DD, YYYY 00:00:00");
        // if (oferta.data_fim) oferta.raw_data_fim = moment(oferta.data_fim).format('YYYY-MM-DD');
        // if (oferta.data_inicio) oferta.raw_data_inicio = moment(oferta.data_inicio).format('YYYY-MM-DD');
      }

      data = {
        empresa: empresa,
        oferta: oferta,
        tema: layout
      };

      View.global('currency', (value) => {
        return Antl.formatNumber(value, { style: 'currency', currency: 'brl' });
      });
      // return data;
      return view.render('modelos/site/hyundai/details', { data: data });
    }
  }
}

module.exports = ShowRoomPageController
