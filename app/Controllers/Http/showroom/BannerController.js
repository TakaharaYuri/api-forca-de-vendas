'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const moment = use('moment');

const model = use('App/Models/showroom/Banner');

/**
 * Resourceful controller for interacting with banners
 */
class BannerController {
  /**
   * Show a list of all banners.
   * GET banners
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();
    let data = await model.query().where('empresa_id', client_id).fetch();
    data = data.toJSON();
    for (const item of data) {
      if (!item.vencido) {
        item.vencido = (item.data_fim < moment().format('YYYY-MM-DD')) ? true : false;
        if (item.vencido) item.status = 0;
      }
    }

    return data;
  }


  /**
   * Create/save a new banner.
   * POST banners
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    const { client_id } = request.all();
    if (data.meta_dados) data.meta_dados = JSON.stringify(data.meta_dados);
    data.empresa_id = client_id;
    const query = await model.create(data);
    return query;
  }

  /**
   * Render a form to update an existing banner.
   * GET banners/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update banner details.
   * PUT or PATCH banners/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only(['id', 'titulo', 'link', 'status', 'imagem_desktop', 'imagem_mobile', 'meta_dados', 'data_inicio', 'data_fim',' empresa_id']);
    const { client_id } = request.all();
    const query = await model.query().where('id', params.id).where('empresa_id', client_id).first();
    if (data.meta_dados) data.meta_dados = JSON.stringify(data.meta_dados);

    query.merge(data);
    await query.save();
    return query;
  }

  /**
   * Delete a banner with id.
   * DELETE banners/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { client_id } = request.all();
    const data = await model.query().where('id', params.id).where('empresa_id', client_id).first();
    return await data.delete();
  }
}

module.exports = BannerController
