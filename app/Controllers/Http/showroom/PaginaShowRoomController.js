'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with paginashowrooms
 */

const model = use('App/Models/showroom/PaginaShowroom');


class PaginaShowroomController {
  /**
   * Show a list of all paginashowrooms.
   * GET paginashowrooms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();
    let data = await model.query().where('empresa_id', client_id).first();
    data.layout = (data.layout) ? JSON.parse(data.layout) : {};
    data.meta_dados = (data.meta_dados) ? JSON.parse(data.meta_dados) : {};
    data.seo = (data.seo) ? JSON.parse(data.seo) : {};
    data.configuracoes = (data.configuracoes) ? JSON.parse(data.configuracoes) : {};
    return data;
  }

  /**
   * Create/save a new paginashowroom.
   * POST paginashowrooms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    const { client_id } = request.all();
    if (data.seo) data.seo = JSON.stringify(data.seo);
    if (data.configuracoes) data.configuracoes = JSON.stringify(data.configuracoes);
    if (data.layout) data.layout = JSON.stringify(data.layout);
    data.empresa_id = client_id;

    const query = await model.create(data);
    return query;
  }

  /**
   * Display a single paginashowroom.
   * GET paginashowrooms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Update paginashowroom details.
   * PUT or PATCH paginashowrooms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const { client_id } = request.all();
    const query = await model.query().where('id', params.id).where('empresa_id', client_id).first();

    if (data.seo) data.seo = JSON.stringify(data.seo);
    if (data.configuracoes) data.configuracoes = JSON.stringify(data.configuracoes);
    if (data.layout) data.layout = JSON.stringify(data.layout);
    if (data.meta_dados) data.meta_dados = JSON.stringify(data.meta_dados);

    query.merge(data);
    await query.save();
    return query;
  }

  /**
   * Delete a paginashowroom with id.
   * DELETE paginashowrooms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = PaginaShowroomController
