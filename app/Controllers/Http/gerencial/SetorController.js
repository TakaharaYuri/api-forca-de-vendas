"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with setors
 */
const model = use("App/Models/gerencial/Setores");
const permissoesModel = use('App/Models/gerencial/Permissoe');
class SetorController {
  /**
   * Show a list of all setors.
   * GET setors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view, auth }) {
    const { client_id } = request.all();

    let data = await model.query().where('empresa_id', client_id).fetch();
    data = data.toJSON();

    for (const item of data) {
      item.permissions = await permissoesModel.query().select('modulos.id', 'modulos.nome').where('id_setor', item.id).innerJoin('modulos', 'id_modulo', 'modulos.id').fetch();
    }

    return data;
  }

  /**
   * Create/save a new setor.
   * POST setors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.only(['id', 'nome']);
    const { client_id } = request.all();
    const permissoes = request.only(['permissions']);
    data.empresa_id = client_id;

    const setor = await model.create(data);

    for (const item of permissoes.permissions) {

      const normalize = {
        id_setor: setor.id,
        id_modulo: item.id
      }

      await permissoesModel.create(normalize);
    }

    return setor;
  }


  /**
   * Update setor details.
   * PUT or PATCH setors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const query = await model.findOrFail(params.id);
    const setor = request.only(['id', 'nome']);
    const permissoes = request.only(['permissions']);

    if (permissoes.permissions) {

      await permissoesModel.query().where('id_setor', setor.id).delete();

      for (const item of permissoes.permissions) {

        const normalize = {
          id_setor: setor.id,
          id_modulo: item.id
        }

        await permissoesModel.create(normalize);
      }
    }

    query.merge(setor);
    await query.save();
    return query;
  }

  /**
   * Delete a setor with id.
   * DELETE setors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const setor = await model.findOrFail(params.id);
    const { client_id } = request.all();
    if (setor.empresa_id == client_id) {
      return await setor.delete()
    }
  }
}

module.exports = SetorController;
