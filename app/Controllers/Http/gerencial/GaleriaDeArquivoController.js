'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const model = use('App/Models/gerencial/GaleriaDeArquivo');

/**
 * Resourceful controller for interacting with galeriadearquivos
 */
class GaleriaDeArquivoController {
  /**
   * Show a list of all galeriadearquivos.
   * GET galeriadearquivos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();
    const data = await model.query()
                            .where('empresa_id', client_id)
                            .orderBy('created_at', 'DESC')
                            .fetch();
    return data;
  }

  async getFilesSelecteds({request}) {
    const { client_id } = request.all();
    let files = request.post();
    let countSeletect = 0;
    let selectedItems = [];

    if (files) {
      files = files.files;
    }

    let data = await model.query()
                          .where('empresa_id', client_id)
                          .orderBy('created_at', 'DESC')
                          .fetch();
    data = data.toJSON();
    for (const item of data) {
      if ((files.filter( file => file.id == item.id).length > 0)){
        item.selected = true;
        countSeletect ++;
        selectedItems.push(item);
      }
    }
    
    return {
      data:data,
      selectCounts: countSeletect,
      selectedItems: selectedItems
    };
  }



  /**
   * Create/save a new galeriadearquivo.
   * POST galeriadearquivos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    let data = request.post();
    const { client_id } = request.all();
    if (data.meta_dados) data.meta_dados = JSON.stringify(data.meta_dados);
    data.empresa_id = client_id;
    data.user_id = auth.user.id;
    const query = await model.create(data);
    return query;
  }

  /**
   * Display a single galeriadearquivo.
   * GET galeriadearquivos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }



  /**
   * Update galeriadearquivo details.
   * PUT or PATCH galeriadearquivos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const { client_id } = request.all();
    const arquivo = await model.query().where(params.id).where('empresa_id', client_id);
    arquivo.merge(data);
    await arquivo.save();
    return arquivo;
  }

  /**
   * Delete a galeriadearquivo with id.
   * DELETE galeriadearquivos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const marca = await model.findOrFail(params.id);
    return await marca.delete();
  }
}

module.exports = GaleriaDeArquivoController
