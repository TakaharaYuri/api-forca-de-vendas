'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with qrcodes
 */

const QRCodeModel = use('App/Models/gerencial/QrCode');
const axios = use('axios');
const Cloudinary = use('App/Services/Cloudinary');
const QRCode = use("qrcode");
const ConfiguracaoModel = use('App/Models/gerencial/Configuracao');
const empresaModel = use('App/Models/gerencial/Empresa');

class QrCodeController {
  /**
   * Show a list of all qrcodes.
   * GET qrcodes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();
    let data = await QRCodeModel.query().where('empresa_id', client_id).fetch();
    return data;
  }


  /**
   * Create/save a new qrcode.
   * POST qrcodes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    const { client_id } = request.all();
    let configuracao = await ConfiguracaoModel.first();
    configuracao = configuracao.toJSON();

    data.empresa_id = client_id;
    const qrcodeData =  await QRCodeModel.create(data);

    const urlQRCode =  `${configuracao.main_url}/qrcode/${qrcodeData.id}?qrcode_id=${qrcodeData.id}&utm_source=forca-de-vendas&utm_medium=qrcode`;
    const qrCodeFile = `public/uploads/${Date.now()}.png`;
    await QRCode.toFile(qrCodeFile, urlQRCode, {width: 720});

    const cloudinaryFile = await Cloudinary.upload(qrCodeFile);
    // qrcodeData.qrcode = ;
    qrcodeData.merge({qrcode:cloudinaryFile.fileName});
    await qrcodeData.save();
    return qrcodeData;
  }

  /**
   * Display a single qrcode.
   * GET qrcodes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Update qrcode details.
   * PUT or PATCH qrcodes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const qrcode = await QRCodeModel.findOrFail(params.id);
    const data = request.only(['id','empresa_id','link','nome','status','meta_dados']);
    qrcode.merge(data);
    await qrcode.save();
    return qrcode;
  }

  /**
   * Delete a qrcode with id.
   * DELETE qrcodes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const qrcode = await QRCodeModel.findOrFail(params.id);
    const { client_id } = request.all();
    if (qrcode.empresa_id == client_id) {
      return await qrcode.delete();
    }
    else {
      return response.status(403).json({
        result: false,
        msg: 'Você não tem permissão para realizar esta ação'
      })
    }
  }

  /**
   * Check if urls is valid.
   * urlChecker qrcodes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async urlChecker({request, response}) {
    const { url } = request.all();
    const requestResult = await axios.get(url);
    return response.status(requestResult.status).send({result:requestResult.status});
  }

  /**
   * Check if urls is valid.
   * urlChecker qrcodes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

   async redirectByQRCode({request, response, params, view})  {
     const qrcode = await QRCodeModel.find( params.id);
     const empresa = await empresaModel.find(qrcode.empresa_id);
     const configuracao = await ConfiguracaoModel.first();

     const data = {qrcode,empresa, configuracao}
    //  return data;
     return view.render('modelos/redirecionar/redirecionar', {data:data});
   }
}

module.exports = QrCodeController
