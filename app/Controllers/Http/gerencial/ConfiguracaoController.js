'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with configuracaos
 */

 const ConfiguracaoModel = use('App/Models/gerencial/Configuracao');

class ConfiguracaoController {
  /**
   * Show a list of all configuracaos.
   * GET configuracaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const data = await ConfiguracaoModel.first();
    return data;
  }

  /**
   * Create/save a new configuracao.
   * POST configuracaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.post();
    const query = await ConfiguracaoModel.create(data);
    return query;
  }

  /**
   * Render a form to update an existing configuracao.
   * GET configuracaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update configuracao details.
   * PUT or PATCH configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const data = request.post();
    const configuracao = await ConfiguracaoModel.findOrFail(data.id);
    configuracao.merge(data);
    await configuracao.save();
    return configuracao;
  }

  /**
   * Delete a configuracao with id.
   * DELETE configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ConfiguracaoController
