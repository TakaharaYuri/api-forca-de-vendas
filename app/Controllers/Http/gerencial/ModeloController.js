'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with modelos
 */

const model = use("App/Models/gerencial/Modelos");
const empresaModel = use('App/Models/gerencial/Empresa');

class ModeloController {
  /**
   * Show a list of all modelos.
   * GET modelos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    let data = await model.query().fetch();
    data = data.toJSON();

    for (const item of data) {
      if (item.imagens) item.imagens = JSON.parse(item.imagens);
    }
    return data;
  }


  /**
 *
 * @param {object} ctx
 * @param {Request} ctx.request
 * @param {Response} ctx.response
 * @param {View} ctx.view
 */
  async show({ params, request, response }) {
    const query = await model.findBy('id', params.id);
    if (query) query.imagens = JSON.parse(query.imagens);
    return query;
  }

  /**
   * Create/save a new modelo.
   * POST modelos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.post();
    if (data.imagens) data.imagens = JSON.stringify(data.imagens);
    const query = await model.create(data);
    return query;
  }

  /**
   * Update modelo details.
   * PUT or PATCH modelos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    if (data.imagens) data.imagens = JSON.stringify(data.imagens);
    const modelo = await model.findOrFail(params.id);

    modelo.merge(data);
    await modelo.save();
    return modelo;
  }

  /**
   * Delete a modelo with id.
   * DELETE modelos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const modelo = await model.findOrFail(params.id);
    return await modelo.delete();
  }

  async modeloByEmpresa({ request, response }) {
    const { client_id } = request.all();

    const empresa = await empresaModel.query().where('id', client_id).first();
    
    if (empresa) {
      const marcas = await model.query().where('marca_id', empresa.marca_id).where('status', 1).fetch();
      return marcas;
    }
    else {
      response.status(500).json({
        result:false,
        msg:'Nenhuma marca encontrada'
      })
    }
  }
}

module.exports = ModeloController
