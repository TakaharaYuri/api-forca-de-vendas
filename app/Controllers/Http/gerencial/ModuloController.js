'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with modulos
 */

const modulosModel = use('App/Models/gerencial/Modulo');

class ModuloController {
  /**
   * Show a list of all modulos.
   * GET modulos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const modulos = modulosModel.query().where('tipo', 'geral').orderBy('created_at','desc').fetch();
    return modulos;
  }

  async modules({ }) {
    let modulos = await modulosModel.query().where('status', 1).whereNull('id_grupo').orderBy('posicao').fetch();
    modulos = modulos.toJSON();

    for (const item of modulos) {
      item.subs = await modulosModel.query().where('id_grupo', item.id).where('status', 1).orderBy('posicao').fetch();
    }
    return modulos;
  }



  /**
   * Create/save a new modulo.
   * POST modulos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    return await modulosModel.create(data);
  }
  /**
   * Update modulo details.
   * PUT or PATCH modulos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    let data = request.post();
    const setor = await modulosModel.findOrFail(params.id);

    setor.merge(data);
    await setor.save();
    return setor;
  }

  /**
   * Delete a modulo with id.
   * DELETE modulos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const setor = await modulosModel.findOrFail(params.id);
    return await setor.delete();
  }
}

module.exports = ModuloController
