'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const model = use("App/Models/gerencial/MarcasModelos");
/**
 * Resourceful controller for interacting with marcasmodelos
 */
class MarcasModeloController {
  /**
   * Show a list of all marcasmodelos.
   * GET marcasmodelos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const data = await model.query().fetch();
    return data;
  }
  /**
   * Create/save a new marcasmodelo.
   * POST marcasmodelos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.post();
    const query = await model.create(data);
    return query;
  }

  /**
   * Update marcasmodelo details.
   * PUT or PATCH marcasmodelos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const marca = await model.findOrFail(params.id);

    marca.merge(data);
    await marca.save();
    return marca;
  }

  /**
   * Delete a marcasmodelo with id.
   * DELETE marcasmodelos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const marca = await model.findOrFail(params.id);
    return await marca.delete();
  }
}

module.exports = MarcasModeloController
