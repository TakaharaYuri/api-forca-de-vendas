"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with empresas
 */
const UsersModel = use("App/Models/User");
const UsuarioEmpresasModel = use("App/Models/gerencial/UsuarioEmpresa");
const UsuarioDadoModel = use("App/Models/gerencial/UsuarioDado");
const EmpresaModel = use("App/Models/gerencial/Empresa");
const dateFormat = require("dateformat");
const FilaUsersEmailModel = use('App/Models/core/FilaUsersEmail');
const Encryption = use("Encryption");
const Event = use("Event");

class UsersController {
  /**
   * Show a list of all empresas.
   * GET empresas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();

    let data = await UsersModel
      .query()
      .select(
        "usuario_dados.nome",
        "users.id",
        "users.email",
        "setores.nome as nome_setor",
        "users.created_at",
        "usuario_empresas.ativo_fila",
        "users.status",
        "usuario_dados.celular"
      )
      .innerJoin("usuario_empresas", "users.id", "usuario_empresas.user_id")
      .innerJoin("usuario_dados", "usuario_dados.user_id", "users.id")
      .innerJoin("setores", "setores.id", "usuario_empresas.setor_id")
      .where("usuario_empresas.empresa_id", client_id)
      .groupBy("usuario_empresas.user_id")
      .fetch();

    return data;
  }

  /**
   * Create/save a new empresa.
   * POST empresas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    // return request.all();
    let {
      email,
      password,
      celular,
      client_id,
      cpf,
      data_nascimento,
      nome,
      setor_id,
      apelido,
      imagem,
    } = request.all();

    let username = email;

    data_nascimento = dateFormat(data_nascimento, "yyyy-mm-dd");

    try {
      const queryUser = await UsersModel.create({ username, email, password });

      try {
        const queryUsuarioDado = await UsuarioDadoModel.create({
          celular,
          cpf,
          data_nascimento,
          nome,
          user_id: queryUser.id,
          apelido,
          imagem,
        });

        const queryUsuarioEmpresa = await UsuarioEmpresasModel.create({
          user_id: queryUser.id,
          setor_id,
          empresa_id: client_id,
        });

        return response.json({
          result: true,
          msg: "Usuario cadastrado com sucesso",
        });
      } catch (error) {
        return response.status(400).json({
          result: false,
          data: error,
        });
      }
    } catch (error) {
      return response.status(400).json({
        result: false,
        data: error,
      });
    }
  }

  /**
  * Update empresa details.
  * PUT or PATCH empresas/:id
  *
  * @param {object} ctx
  * @param {Request} ctx.request
  * @param {Response} ctx.response
  */

  async sendInvitation({ request, response }) {
    let { email, setor_id, client_id, nivel } = request.all();
    let username = email;
    let password = Encryption.encrypt('server');

    const checkUserExists = await UsersModel.findBy('email', email);
    if (checkUserExists) {

      const checkUserExistsEmpresa = await UsuarioEmpresasModel.query()
        .where('user_id', checkUserExists.id)
        .where('empresa_id', client_id)
        .first();
      if (!checkUserExistsEmpresa) {
        const queryUsuarioEmpresa = await UsuarioEmpresasModel.create({
          user_id: checkUserExists.id,
          setor_id,
          empresa_id: client_id,
        });

        const queryInvitation = await FilaUsersEmailModel.create({
          user_id: checkUserExists.id,
          empresa_id: client_id,
          tipo: 'inclusion',
          modelo: 'new_inclusion'
        });
      }
      else {
        return response.status(403).json({
          result: false,
          msg: "Este usuário já está cadastrado na sua unidade",
        });
      }

      // Event.fire("new:sendUserInclusion", checkUserExists.id);

      return response.json({
        result: true,
        msg: "Foi enviado um convite para que este usuário junte-se a sua unidade",
      });

    }
    else {
      const queryUser = await UsersModel.create({ username, email, password, status: 2, nivel });
      if (queryUser) {
        const queryUsuarioDado = await UsuarioDadoModel.create({ user_id: queryUser.id });

        const queryUsuarioEmpresa = await UsuarioEmpresasModel.create({
          user_id: queryUser.id,
          setor_id,
          empresa_id: client_id,
        });

        const queryInvitation = await FilaUsersEmailModel.create({
          user_id: queryUser.id,
          empresa_id: client_id,
          tipo: 'invitation',
          modelo: 'new_invitation'
        });


        Event.fire("new:sendUserInvite", queryUser.id);
        return response.json({
          result: true,
          msg: "Usuario cadastrado com sucesso",
        });
      }
      else {
        return response.status(500).json({
          result: false,
          msg: "Ocorreu um problema ao enviar este convite",
        });
      }
    }
  }

  /**
   * Update empresa details.
   * PUT or PATCH empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    // return request.post();
    const { client_id } = request.all();
    const _usuarioDados = request.only(["celular", "nome", "imagem", "apelido", "syonet_usuario", "syonet_senha"]);
    const _userDados = request.only(['email', 'password', 'status']);
    const _usuarioEmpresa = request.only(['ativo_fila', 'setor_id']);

    if (_usuarioDados.data_nascimento) _usuarioDados.data_nascimento = dateFormat(_usuarioDados.data_nascimento, "yyyy-mm-dd");
    if (_usuarioDados.data_admissao) _usuarioDados.data_admissao = dateFormat(_usuarioDados.data_admissao, "yyyy-mm-dd");

    const user = await UsersModel.find(params.id);
    const usuario_dados = await UsuarioDadoModel.query().where('user_id', user.id).first();
    const usuario_empresa = await UsuarioEmpresasModel.query().where('user_id', user.id).where('empresa_id', client_id).first();

    usuario_dados.merge(_usuarioDados);
    await usuario_dados.save();

    user.merge(_userDados);
    await user.save();

    usuario_empresa.merge(_usuarioEmpresa);
    await usuario_empresa.save();

    return { success: true };

  }


  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const id = params.id;
    let data = await UsersModel
      .query()
      .select(
        "usuario_dados.nome",
        "usuario_dados.instagram",
        "usuario_dados.apelido",
        "usuario_dados.celular",
        "usuario_dados.imagem",
        "usuario_dados.cpf",
        "usuario_dados.data_nascimento",
        "users.id",
        "users.email",
        "users.created_at",
        "usuario_empresas.ativo_fila",
        "usuario_empresas.setor_id",
        "users.status",
        "usuario_dados.celular",
        "usuario_empresas.empresa_id",
        "users.status"
      )
      .innerJoin("usuario_empresas", "users.id", "usuario_empresas.user_id")
      .innerJoin("usuario_dados", "usuario_dados.user_id", "users.id")
      .innerJoin("setores", "setores.empresa_id", "usuario_empresas.empresa_id")
      .where("users.id", id)
      .groupBy("usuario_empresas.user_id")
      .first();

    data.data_nascimento = dateFormat(data.data_nascimento, "dd/mm/yyyy");

    return data;
    // return await empresaModel.getBy("id", id);
  }


  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async mailMount({ params, request, response, view }) {
    const queryInvites = await FilaUsersEmailModel.query().where('user_id', params.id).where('status', 0).first();
    const invite = queryInvites.toJSON();
    invite.user = await UsersModel.find(invite.user_id);
    invite.empresa = await EmpresaModel.find(invite.empresa_id);

    invite.empresa = invite.empresa.toJSON();
    invite.user = invite.user.toJSON();
    invite.json = JSON.stringify(invite);
    // return invite;
    return view.render("emails/new-invite", { data: invite });
  }


  /**
   * Delete a empresa with id.
   * DELETE empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response, auth }) {
    if (auth.user.nivel == 'manager' || auth.user.nivel == 'admin') {
      const user = await UsersModel.findOrFail(params.id);
      await user.delete();
      return response.json({ result: true })
    }
    else {
      return response.status(403).json({
        result: false,
        msg: 'Você não tem permissão para realizar esta ação'
      })
    }
  }

  /**
   * Delete a empresa with id.
   * DELETE empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getProfile({ params, request, response, auth }) {
    const { client_id } = request.all();

    let data = await UsersModel
      .query()
      .select(
        "usuario_dados.nome",
        "users.id",
        "users.email",
        "setores.nome as nome_setor",
        "users.created_at",
        "usuario_empresas.ativo_fila",
        "users.status",
        "usuario_dados.celular",
        "usuario_dados.imagem",
        "usuario_dados.cpf",
        "usuario_dados.apelido",
        "usuario_dados.data_nascimento",
        "usuario_dados.bio",
        "usuario_dados.data_admissao"
      )
      .innerJoin("usuario_empresas", "users.id", "usuario_empresas.user_id")
      .innerJoin("usuario_dados", "usuario_dados.user_id", "users.id")
      .innerJoin("setores", "setores.id", "usuario_empresas.setor_id")
      .where("usuario_empresas.empresa_id", client_id)
      .where("users.id", auth.user.id)
      .groupBy("usuario_empresas.user_id")
      .first();

    return data;
  }

  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async updateProfile({ params, request, response, auth }) {
    const usuarioDados = request.only(["celular", "nome", "imagem", "apelido", "data_nascimento", "bio", "data_admissao"]);
    usuarioDados.data_nascimento = dateFormat(usuarioDados.data_nascimento, "yyyy-mm-dd");
    usuarioDados.data_admissao = dateFormat(usuarioDados.data_admissao, "yyyy-mm-dd");

    const userDados = request.only(['email', 'password']);

    const usuario = await UsuarioDadoModel.findOrFail(params.user_id);
    const user = await UsersModel.findOrFail(params.user_id);

    if (auth.user.id == params.user_id) {
      usuario.merge(usuarioDados);
      user.merge(userDados);
      await user.save();
      await usuario.save();

      return usuario;
    }
  }

  /**
  *
  * @param {object} ctx
  * @param {Request} ctx.request
  * @param {Response} ctx.response
  */
  async checkEmailExists({ params, request, response }) {
    const { email, client_id } = request.all();
    if (email) {
      const usuario = await UsersModel.query()
        // .innerJoin('usuario_empresas', 'users.id', 'usuario_empresas.user_id')
        .where('email', email)
        .first();

      if (usuario) {

        const usuarioEmpresa = await UsuarioEmpresasModel.query().where('user_id', usuario.id).where('empresa_id', client_id).first();
        if (!usuarioEmpresa) {
          if (usuario.status == 0) {
            return {
              reason: 'blocked',
              msg: 'Esta conta encontra-se bloqueada para receber novos convites.',
              result: true,
            }
          }
          else if (usuario.status == 1) {
            return {
              reason: 'user_exists',
              msg: 'Este usuário já está cadastrado em nossa base de dados, iremos apenas enviar o convite para que ele ingresse na sua empresa',
              result: true
            }
          }
          else if (usuario.status == 2) {
            return {
              reason: 'not_activate',
              msg: 'Esta conta ainda não foi ativada, por este motivo não pode receber novos convites. Reenvie o convite anterior para que ela possa ser ativada.',
              result: true
            }
          }
          else {
            return {
              reason: 'error',
              msg: 'Ocorreu um erro ao consultar os dados deste usuário, tente novamente.',
              result: true
            }
          }
        }
        else {
          return {
            reason: 'user_same_exists',
            msg: 'Já existe um usuário com este mesmo E-mail na sua unidade.',
            result: true,
          }
        }
      }
      else {
        return {
          result: false,
          msg: 'E-mail válido'
        }
      }
    }
    else {
      return {
        result: false,
        msg: 'Nenhum e-mail informado'
      }
    }
  }

  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async validateInvitation({ params, request, response }) {
    const { key, ref } = request.all();
    const userId = Encryption.decrypt(key);
    const usuario = await UsersModel.query()
      .select('users.*', 'email', 'nome', 'cpf', 'celular', 'empresa_id')
      .join('usuario_empresas', 'users.id', 'usuario_empresas.user_id')
      .join('usuario_dados', 'usuario_dados.user_id', 'usuario_empresas.user_id')
      .where('users.id', userId).where('users.status', 2).first();
    // 

    if (usuario) {
      const empresa = await EmpresaModel.query().select('id', 'nome', 'logo').where('id', usuario.empresa_id).first();

      if (empresa) {
        return response.json({
          empresa: empresa,
          usuario: usuario
        })
      }
      else {
        return response.status(403).json({
          result: false,
          msg: 'Não foi possível validar o empresa informado'
        })
      }
    }
    else {
      return response.status(403).json({
        result: false,
        msg: 'Não foi possível validar a usuario informada'
      })
    }
  }

  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async saveProfileInvitation({ request, response }) {
    const { nome, celular, cpf, password, id } = request.all();
    const usuario = await UsersModel.find(id);
    if (usuario) {
      const usuarioDados = await UsuarioDadoModel.findBy('user_id', usuario.id);
      usuarioDados.merge({ nome, celular, cpf });
      usuario.merge({ password });

      await usuario.save();
      await usuarioDados.save();
      return response.json({
        result: true,
        response: 'Dados salvos com sucesso'
      })

    }
    else {
      return response.status(403).json({
        result: false,
        msg: 'Não foi possível validar o usuário'
      })
    }
  }

  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async finalizeInvitation({ request, response }) {
    const { id, apelido, instagram, facebook, sexo, imagem } = request.all();
    const usuario = await UsersModel.find(id);
    if (usuario) {
      const usuarioDados = await UsuarioDadoModel.findBy('user_id', usuario.id);
      usuarioDados.merge({ apelido, instagram, facebook, sexo, imagem });
      usuario.merge({ status: 1 });

      await usuario.save();
      await usuarioDados.save();
      return response.json({
        result: true,
        response: 'Dados salvos com sucesso'
      });

    }
    else {
      return response.status(403).json({
        result: false,
        msg: 'Não foi possível validar o usuário'
      })
    }
  }
}


module.exports = UsersController;
