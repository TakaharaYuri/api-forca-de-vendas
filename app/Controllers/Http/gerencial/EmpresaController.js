"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with empresas
 */

const db = use("Database");
const empresaModel = use("App/Models/gerencial/Empresa");
const QRCode = use("qrcode");
const Encryption = use("Encryption");
const Event = use("Event");
const fs = use('fs');
const Cloudinary = use('App/Services/Cloudinary')


const ConfiguracaoModel = use('App/Models/gerencial/Configuracao');

class EmpresaController {
    /**
     * Show a list of all empresas.
     * GET empresas
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async index({ request, params, auth }) {
        const { page, search = '' } = request.get();
        console.log(auth.user);
        let query = empresaModel.query("empresas")
                                        .select("empresas.*")
                                        .innerJoin("usuario_empresas", "usuario_empresas.empresa_id", "empresas.id")
                                        .where("usuario_empresas.user_id", auth.user.id)
                                        .where("empresas.status", 1);
        if (search != '') {
            query = query.whereRaw(`(empresas.nome LIKE '%${search}%')`)
        }

        query = await query.orderBy("empresas.id", "DESC").paginate(page, 7);
        return query;
    }

    /**
     * Render a form to be used for creating a new empresa.
     * GET empresas/create
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */

    /**
     * Create/save a new empresa.
     * POST empresas
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async store({ request, response, auth }) {
        const configuracao = await ConfiguracaoModel.first();
        const data = request.post();
        const empresa = await empresaModel.create(data);
        const urlQRCode = `${configuracao.main_url}/redirecionar/${empresa.id}`;
        const qrCodeFile = `public/uploads/${Date.now()}.png`;

        await QRCode.toFile(qrCodeFile, urlQRCode, {
            width: 720,
        });

        const cloudinaryFile = await Cloudinary.upload(qrCodeFile);

        empresa.qrcode = cloudinaryFile.fileName;
        empresa.qrcode_reference = cloudinaryFile.fileName;

        await empresa.save();
        Event.fire("new:empresaUsuarios", empresa, await auth.user);
        return empresa;
    }

    /**
     * Display a single empresa.
     * GET empresas/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     * @param {View} ctx.view
     */
    async show({ params, request, response, view }) {
        const id = params.id;
        const empresa = await empresaModel.query().where("id", id).where('status', 1).first();
        return empresa;
    }

    /**
     * Update empresa details.
     * PUT or PATCH empresas/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async update({ params, request, response }) {
        const empresa = await empresaModel.findOrFail(params.id);
        const data = request.only([
            "cep",
            "cidade",
            "cnpj",
            "complemento",
            "estado",
            "key",
            "logo",
            "meta_dados",
            "rua",
            "razao_social",
            "qrcode_reference",
            "qrcode",
            "urls",
            "tipo_fila",
            "nome",
            "identificador",
            "tipo_concessionaria",
            "id",
            "marca_id",
            "host"
        ]);

        if (!data.key || data.key == null) {
            data.key = Encryption.encrypt(data.id);
        }
        empresa.merge(data);
        await empresa.save();
        return empresa;
    }

    /**
     * Delete a empresa with id.
     * DELETE empresas/:id
     *
     * @param {object} ctx
     * @param {Request} ctx.request
     * @param {Response} ctx.response
     */
    async destroy({ params, request, response }) {
        const empresa = await empresaModel.findBy('id', params.id);
        empresa.merge({ status: 0 });
        empresa.save();
        return empresa;
    }

    async qrCodeGenerate({ params, request, response }) {
        const empresa = await empresaModel.findOrFail(params.id);
        const configuracao = await ConfiguracaoModel.first();
        // const file = `public/uploads/${empresa.qrcode}.png`

        // if (await fs.existsSync(file)) {
        //   await fs.unlinkSync(file);
        // }

        const urlQRCode = `${configuracao.main_url}/redirecionar/${empresa.id}`;
        const qrCodeFile = `public/uploads/${Date.now()}.png`;

        await QRCode.toFile(qrCodeFile, urlQRCode, {
            width: 720,
        });
        const cloudinaryFile = await Cloudinary.upload(qrCodeFile);
        empresa.qrcode = cloudinaryFile.fileName;
        empresa.qrcode_reference = cloudinaryFile.fileName;
        await empresa.save();

        return empresa;
    }
}

module.exports = EmpresaController;