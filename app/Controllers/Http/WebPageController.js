"use strict";
const edge = use('edge.js');
const moment = use('moment');
const EmpresaModel = use("App/Models/gerencial/Empresa");
const UserModel = use("App/Models/User");
const PaginaContatoModel = use('App/Models/marketing/PaginaContato');
const FormularioModel = use('App/Models/marketing/Formulario');
const CategoriaProdutoModel = use('App/Models/estoque/Categoria');
const Env = use('Env');


class WebPageController {
  async index({ params, view }) {
    let empresa = await EmpresaModel.findBy("identificador", params.identificador);
    const paginaContato = await PaginaContatoModel.findBy('empresa_id', empresa.id);
    var _layout;
    const _formulario = await FormularioModel.query()
      .where('empresa_id', empresa.id)
      .where('id', paginaContato.formulario_id)
      .with('items', (builder) => {
        builder.orderBy('position_index', 'ASC')
          .where('enabled', 1)
          .setHidden(['created_at', 'updated_at', 'raw_data', 'formulario_id']);
      })
      .setHidden(['id', 'status', 'empresa_id', 'created_at', 'updated_at'])
      .first();

    const formulariosJson = _formulario.toJSON();
    formulariosJson.items.map(item => {
      item.options = JSON.parse(item.options);
      item.conditions = JSON.parse(item.conditions);
      item.selected_options = JSON.parse(item.selected_options);
    });

    const vendedor = await UserModel.query()
      .select(
        "usuario_dados.nome",
        "users.id",
        "users.email",
        "setores.nome as nome_setor",
        "users.created_at",
        "usuario_empresas.ativo_fila",
        "users.status",
        "usuario_dados.celular",
        "usuario_dados.imagem"
      )
      .innerJoin("usuario_empresas", "users.id", "usuario_empresas.user_id")
      .innerJoin("usuario_dados", "usuario_dados.user_id", "users.id")
      .innerJoin("setores", "setores.empresa_id", "usuario_empresas.empresa_id")
      .where('users.status', 1)
      .where('usuario_empresas.ativo_fila', 1)
      .where('usuario_empresas.empresa_id', empresa.id)
      .groupBy("usuario_empresas.user_id")
      .first();


    vendedor.celular = vendedor.celular.replace(/[^A-Z0-9]/ig, "_");
    empresa.vendedor = vendedor;
    empresa.formulario = formulariosJson;
    empresa.host = Env.get('APP_URL');

    const categoria_produtos = await CategoriaProdutoModel.query().with('produtos').where('empresa_id', empresa.id).fetch();
    empresa.categoria_produtos = categoria_produtos.toJSON();

    if (paginaContato) {
      paginaContato.layout = JSON.parse(paginaContato.layout);
      paginaContato.configuracoes = JSON.parse(paginaContato.configuracoes);
      paginaContato.seo = JSON.parse(paginaContato.seo);
      empresa.pagina_contato = paginaContato;
    }
    // console.log(empresa);
    // return empresa;
    if (empresa.configuracoes && empresa.configuracoes.layout == '2r') _layout = view.render("modelos/landing-pages/2r", { empresa: empresa.toJSON() });
    if (empresa.configuracoes && empresa.configuracoes.layout == '4r') _layout = view.render("modelos/landing-pages/4r", { empresa: empresa.toJSON() });
    else _layout = view.render("modelos/landing-pages/dynamic", { empresa: empresa.toJSON() });
    return _layout;
  }

}

module.exports = WebPageController;
