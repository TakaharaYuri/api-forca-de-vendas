'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with leads
 */

const LeadsModel = use('App/Models/marketing/Lead');
class LeadController {
  /**
   * Show a list of all leads.
   * GET leads
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, auth }) {
    const { client_id, page, folder } = request.all();
    let leads;

    if (auth.user.nivel == 'manager' || auth.user.nivel == 'admin') {
      leads = LeadsModel.query()
                              .with('contato')
                              .with('produto')
                              .with('etiquetas', (builder)=>{
                                builder.orderBy('created_at','desc');
                              })
                              .with('vendedor', (builder) => {
                                builder.setHidden(['data_nascimento', 'imagem', 'apelido','instagram','celular','cpf','user_id', 'created_at','updated_at','sexo','syonet_configuracoes','syonet_senha','syonet_usuario']);
                              })
                              .where('leads.empresa_id', client_id);
      if (folder) {
        leads = leads.where('id_pasta', folder);
      }

      leads = await leads.paginate(page, 10);
    }
    else if (auth.user.nivel == 'user') {
      leads = await LeadsModel.query()
                            .with('contato')
                            .with('produto')
                            .with('vendedor', (builder) => {
                              builder.setHidden(['data_nascimento', 'imagem', 'apelido','instagram','celular','cpf','user_id', 'created_at','updated_at','sexo','syonet_configuracoes','syonet_senha','syonet_usuario']);
                            })
                            .where('leads.empresa_id', client_id)
                            .where('leads.user_id', auth.user.id)
                            .orderBy('created_at','DESC');

      if (folder) {
        leads = leads.where('id_pasta', folder);
      }

      leads = await leads.paginate(page, 10);
    }

    return leads;
  }

  /**
   * Render a form to be used for creating a new lead.
   * GET leads/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new lead.
   * POST leads
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single lead.
   * GET leads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing lead.
   * GET leads/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update lead details.
   * PUT or PATCH leads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a lead with id.
   * DELETE leads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = LeadController
