'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with leadspastas
 */

const model = use('App/Models/marketing/LeadsPasta');
class LeadsPastaController {
  /**
   * Show a list of all leadspastas.
   * GET leadspastas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { client_id } = request.all();

    let data = await model.query().where('empresa_id', client_id).orderBy('created_at','desc').fetch();
    return data;
  }

 
  /**
   * Create/save a new leadspasta.
   * POST leadspastas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    let data = request.only(['id', 'nome']);
    const { client_id } = request.all();
    data.empresa_id = client_id;
    const pasta = await model.create(data);
    return pasta;
  }

  /**
   * Display a single leadspasta.
   * GET leadspastas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update leadspasta details.
   * PUT or PATCH leadspastas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const query = await model.findOrFail(params.id);
    const data = request.only(['id', 'nome']);
    query.merge(data);
    await query.save();
    return query;
  }

  /**
   * Delete a leadspasta with id.
   * DELETE leadspastas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const query = await model.findOrFail(params.id);
    const { client_id } = request.all();
    if (query.empresa_id == client_id) {
      return await query.delete();
    }
  }
}

module.exports = LeadsPastaController
