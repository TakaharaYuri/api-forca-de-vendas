'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with formularios
 */

const FormularioModel = use('App/Models/marketing/Formulario');

class FormularioController {
  /**
   * Show a list of all formularios.
   * GET formularios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { client_id } = request.all();
    const formularios = await FormularioModel.query().orderBy('created_at', 'desc').where('empresa_id',client_id).fetch();
    return formularios;
  }

  /**
   * Create/save a new formulario.
   * POST formularios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { client_id } = request.all();
    const data = request.only(['nome', 'imagem','status']);
    data.empresa_id = client_id;

    const query = await FormularioModel.create(data);
    return query;
  }

  /**
   * Display a single formulario.
   * GET formularios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update formulario details.
   * PUT or PATCH formularios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const data = request.only(['nome', 'imagem','status']);
    const formulario = await FormularioModel.find(params.id);
    formulario.merge(data);
    formulario.save();
    return formulario;
  }

  /**
   * Delete a formulario with id.
   * DELETE formularios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const { client_id } = request.all();
    const formulario = await FormularioModel.query().where('id', params.id).where('empresa_id', client_id).delete();
    return formulario;
  }
}

module.exports = FormularioController
