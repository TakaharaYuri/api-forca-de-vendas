'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with etiquetas
 */

const model = use('App/Models/marketing/Etiqueta');
const leadModel = use('App/Models/marketing/Lead');
const etiquetasLeadModel = use('App/Models/marketing/EtiquetasLead');

class EtiquetaController {
  /**
   * Show a list of all etiquetas.
   * GET etiquetas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id } = request.all();
    let data = await model.query().where('empresa_id', client_id).orderBy('created_at', 'desc').fetch();
    return data;
  }


  /**
   * Create/save a new etiqueta.
   * POST etiquetas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.only(['nome', 'cor', 'icone']);
    const { client_id } = request.all();
    data.empresa_id = client_id;
    const etiqueta = await model.create(data);
    return etiqueta;
  }

  /**
   * Display a single etiqueta.
   * GET etiquetas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async getByLead({ params, request}) {
    const { client_id } = request.all();
    let data = await model.query()
                          .leftJoin('etiquetas_leads', 'etiquetas.id','etiquetas_leads.etiqueta_id')
                          .where('empresa_id', client_id)
                          .where('id_lead', params.id)
                          .orderBy('etiquetas.created_at','desc')
                          .groupBy('etiquetas.id')
                          .fetch();
    return data;
  }

  async addEtiquetaInLead({params}) {
    let etiquetaLead;
    const checkEtiquetaLeadExists = await etiquetasLeadModel.query()
                                                            .where('etiqueta_id', params.etiqueta_id)
                                                            .where('lead_id', params.lead_id)
                                                            .first();
    if (checkEtiquetaLeadExists) {
      etiquetaLead = await checkEtiquetaLeadExists.delete();
    }
    else {
      const data = {
        lead_id:params.lead_id,
        etiqueta_id: params.etiqueta_id
      }
      etiquetaLead = await etiquetasLeadModel.create(data);
    }

    return etiquetaLead;
  }

  /**
   * Update etiqueta details.
   * PUT or PATCH etiquetas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const query = await model.findOrFail(params.id);
    let data = request.only(['id', 'nome', 'cor', 'icone']);
    query.merge(data);
    await query.save();
    return query;
  }

  /**
   * Delete a etiqueta with id.
   * DELETE etiquetas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const query = await model.findOrFail(params.id);
    const { client_id } = request.all();
    if (query.empresa_id == client_id) {
      return await query.delete();
    }
  }
}

module.exports = EtiquetaController
