'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with paginacontatoes
 */
const PaginaContatoModel = use('App/Models/marketing/PaginaContato');

class PaginaContatoController {
  /**
   * Show a list of all paginacontatoes.
   * GET paginacontatoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    const { client_id } = request.all();
    const data = await PaginaContatoModel.query().where('empresa_id', client_id).first();

    if (data && data.seo) data.seo = JSON.parse(data.seo);
    if (data && data.configuracoes) data.configuracoes = JSON.parse(data.configuracoes);
    if (data && data.layout) data.layout = JSON.parse(data.layout);
    return data;
  }

  /**
   * Create/save a new paginacontato.
   * POST paginacontatoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.post();
    const { client_id } = request.all();

    data.empresa_id = client_id;
    data.seo = JSON.stringify(data.seo);
    data.configuracoes = JSON.stringify(data.configuracoes);
    data.layout = JSON.stringify(data.layout);

    const pagina = await PaginaContatoModel.create(data);
    return pagina;
  }


  /**
   * Update paginacontato details.
   * PUT or PATCH paginacontatoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const pagina = await PaginaContatoModel.findOrFail(params.id);
    const data = request.post();
    data.seo = JSON.stringify(data.seo);
    data.configuracoes = JSON.stringify(data.configuracoes);
    data.layout = JSON.stringify(data.layout);
    pagina.merge(data);
    await pagina.save();
    return pagina;

  }

  /**
   * Delete a paginacontato with id.
   * DELETE paginacontatoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = PaginaContatoController
