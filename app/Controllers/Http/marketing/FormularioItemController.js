"use strict";

const { raw } = require("mysql");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with formularios
 */

const FormularioItemsModel = use("App/Models/marketing/FormularioItem");

class FormularioItemController {
  /**
   * Show a list of all jornadaitems.
   * GET jornadaitems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { client_id, formulario_id } = request.all();
    var items = await FormularioItemsModel.query()
                                          .where('formulario_id', formulario_id)
                                          .orderBy('position_index','asc')
                                          .fetch();
    const jsonItems = items.toJSON();
    jsonItems.map(item => {
      item.options = JSON.parse(item.options);
      item.conditions = JSON.parse(item.conditions);
      item.selected_options = JSON.parse(item.selected_options);
      // item.raw_data = JSON.parse(item);
    });

    return jsonItems;
  }

  /**
   * Create/save a new jornadaitem.
   * POST jornadaitems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const { etapas, formulario_id, client_id } = request.all();

    etapas.map(async (item) => {
      item.options = JSON.stringify(item.options);
      item.conditions = JSON.stringify(item.conditions);
      item.selected_options = JSON.stringify(item.selected_options);
      item.raw_data = JSON.stringify(item);
      item.formulario_id = Number(formulario_id);

      await FormularioItemsModel.create(item);
    });

    return etapas;

  }

  /**
   * Display a single jornadaitem.
   * GET jornadaitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    
  }

  /**
   * Update jornadaitem details.
   * PUT or PATCH jornadaitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { etapas, formulario_id, client_id } = request.all();
    etapas.forEach( async (item, index)=>{
      item.position_index = index;
      item.options = JSON.stringify(item.options);
      item.conditions = JSON.stringify(item.conditions);
      item.selected_options = JSON.stringify(item.selected_options);
      // item.raw_data = JSON.stringify(item);
      if (item.id) {
        const etapa = await FormularioItemsModel.find(item.id);
        etapa.merge(item);
        etapa.save();
      }
      else {
        item.formulario_id = formulario_id;
        await FormularioItemsModel.create(item);
      }
    });

    return {result:true};
  }

  /**
   * Delete a jornadaitem with id.
   * DELETE jornadaitems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { formulario_id, client_id } = request.all();
    const item = await FormularioItemsModel.query()
                                           .where('formulario_id', formulario_id)
                                           .where('id', params.id)
                                           .delete();
    return item;
  }
}

module.exports = FormularioItemController;
