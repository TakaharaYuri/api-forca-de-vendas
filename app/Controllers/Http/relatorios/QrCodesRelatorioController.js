'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with qrcodesrelatorios
 */
const QRCodeRelatorioModel = use('App/Models/relatorios/QrCodesRelatorio');
const DeviceDetector = use('device-detector-js');
const deviceDetector = new DeviceDetector();
const md5 = use('js-md5');

class QrCodesRelatorioController {
  /**
   * Show a list of all qrcodesrelatorios.
   * GET qrcodesrelatorios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    
  }

  /**
   * Render a form to be used for creating a new qrcodesrelatorio.
   * GET qrcodesrelatorios/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new qrcodesrelatorio.
   * POST qrcodesrelatorios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single qrcodesrelatorio.
   * GET qrcodesrelatorios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const visitas = await QRCodeRelatorioModel.query().select('device','so','created_at','city','region','navigator').where('qrcode_id', params.id).orderBy('updated_at','DESC').limit(10).fetch();
    const qtd_visitas = await QRCodeRelatorioModel.query().where('qrcode_id', params.id).orderBy('updated_at','DESC').count('* as total');
    const dispositivos_utilizados = await QRCodeRelatorioModel.query().select('device','so','navigator').where('qrcode_id', params.id).groupBy('device').orderBy('total','DESC').count('* as total');
    const visitas_unicas = await QRCodeRelatorioModel.query().select('device','so','created_at','city','region','navigator').where('qrcode_id', params.id).groupBy('ip').orderBy('updated_at','DESC').limit(10).count('* as total');
    const qtd_visitas_unicas = await QRCodeRelatorioModel.query().where('qrcode_id', params.id).groupBy('ip').orderBy('total','DESC').count('* as total');
    const visitas_cidade = await QRCodeRelatorioModel.query().select('device','so','created_at','city','region','navigator').where('qrcode_id', params.id).groupBy('city').orderBy('total','DESC').count('* as total');

    const data = {
      visitas,
      visitas_unicas,
      dispositivos_utilizados,
      visitas_cidade,
      qtd_visitas,
      qtd_visitas_unicas
    }
    return data;
  }

  /**
   * Update qrcodesrelatorio details.
   * PUT or PATCH qrcodesrelatorios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a qrcodesrelatorio with id.
   * DELETE qrcodesrelatorios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  /**
   * Delete a qrcodesrelatorio with id.
   * DELETE qrcodesrelatorios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async ping({ params, request, response }) {
    const requestData = request.all();
    const _headers = request.headers();
    const _hostname = request.hostname();
    const device = deviceDetector.parse(_headers['user-agent']);
    const data = {
      qrcode_id: requestData.qrcode_id,
      url: _headers['referer'],
      device_raw: JSON.stringify(device),
      request_raw: JSON.stringify(requestData),
      headers: JSON.stringify(_headers),
      navigator: device.client.name,
      so: device.os.name,
      device: device.device.type,
      user_agent: _headers['user-agent'],
      hash: md5(JSON.stringify(_headers)),
      city:requestData.city,
      ip:requestData.ip,
      country:requestData.country,
      region:requestData.region,
      utm_source: requestData.utm_source,
      utm_campaign: requestData.utm_campaign,
      utm_medium: requestData.utm_medium,
      utm_term: requestData.utm_term,
      utm_origin: requestData.utm_origin
    }

    if (requestData.id) {
      const dataExists = await QRCodeRelatorioModel.findBy('id', requestData.id);
      if (dataExists) {
        dataExists.merge(data);
        const query = await dataExists.save();
        return query;
      }
    }
    else {
      const query = await QRCodeRelatorioModel.create(data);
      return query;
    }
  }
}

module.exports = QrCodesRelatorioController
