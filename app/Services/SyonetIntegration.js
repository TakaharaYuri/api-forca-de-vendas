"use strict";
const soap = use('soap');

module.exports = {
  gerarEvento: async (dados, options) => {
    let url = 'https://linhares.syonet.com/CollaborativeWS-CollaborativeWS/CollaborativeEventoService?wsdl';
    if (dados.url) {
      url = dados.url;
    }
    delete dados.url;
    console.log('[URL] ->', url);
    console.log(`options`, options);
    console.log('dados', dados);
    const soapResult = await new Promise(resolve => {
      try {
        soap.createClient(url, options, (err, client) => {
          client.gerarEventoV2(dados, (err, result) => {
            if (result.return) {
              console.log('[SYONET] -> success', result);
              resolve(JSON.parse(result.return));
            }
            else {
              console.log('[SYONET] -> error', result);
              resolve({result:false, syonet: result});
            }
          });
        });
      } catch (error) {
        reject(error);
      }
    });

    return soapResult;
  }
}
