"use strict";

const http = use('axios');
const headers = {
  "Origin": "https://veiculos.fipe.org.br",
  "Accept": "application/json, text/javascript, */*; q=0.01",
  "X-Requested-With": "XMLHttpRequest",
  "Referer": "https://veiculos.fipe.org.br/",
  "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36",
  "Host": "veiculos.fipe.org.br",
  "Content-Type": "application/x-www-form-urlencoded"
}
module.exports = {
  request: async (method, data) => {
    const resultResponse = await new Promise((resolve, reject) => {

      http.post(`https://veiculos.fipe.org.br/api/veiculos/${method}`, data, headers)
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      })
    });

    return resultResponse;
  },
}