"use strict";

module.exports = {
  cleanString(string) {
    return string.replace(/[^\w]/gi, "");
  },

  moneyFormat(value) {
    return new Intl.NumberFormat('pt', { 
      style: 'currency', 
      currency: 'BRL',
    }).format(value);
  }
}