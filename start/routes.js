'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
// Route.post('/api/register','AuthController.register');
Route.post('/api/login','AuthController.login');
Route.post('/api/upload', 'core/UploadController.uploadImage');
Route.get('/api/file/:file', 'core/UploadController.showFile');
Route.get('/redirecionar/:id', 'IntegracaoController.redirectByQRCode');
Route.get('/api/redirecionar/:id', 'IntegracaoController.redirectByQRCode');
Route.get('/loja/:identificador', 'WebPageController.index');
Route.get('/qrcode/:id', 'gerencial/QrCodeController.redirectByQRCode');
Route.post('/api/integracoes/syonet', 'integracoes/SyonetController.gerar_evento');
Route.post('/api/integracoes/syonet-wordpress', 'integracoes/SyonetController.gerar_evento_wordPress');
Route.resource('/portal/seminovos', 'seminovos/PageSeminovoController');

Route.post('/api/integracoes/gerador-imagens', 'integracoes/GeradorImagenController.generate');
Route.get('/api/integracoes/gerador-imagens', 'integracoes/GeradorImagenController.index');

Route.get('/', 'showroom/ShowRoomPageController.showroom');
Route.get('/oferta/:url', 'showroom/ShowRoomPageController.details');

Route.get('/api/integracoes/fipe/consultar_paca/:placa', 'integracoes/FipeController.consultarPlaca');

Route.group(() => {
    Route.post('/configuracoes', 'IntegracaoController.configuracoes');
    Route.get('/registra_acesso', 'IntegracaoController.registraAcesso');
    Route.post('/lead/novo', 'IntegracaoController.novoLeadDinamico');
    Route.get('/ping', 'relatorios/QrCodesRelatorioController.ping');
}).prefix('integrador');


Route.group(() => {
    Route.get('/empresa', 'gerencial/UsersController.sendInvitation');
    Route.post('/validar', 'gerencial/UsersController.validateInvitation');
    Route.post('/perfil', 'gerencial/UsersController.saveProfileInvitation');
    Route.post('/finalizar', 'gerencial/UsersController.finalizeInvitation');
}).prefix('api/convite');


Route.group(() => {
    // Actions
    Route.post('/acoes/enviar-convite', 'gerencial/UsersController.sendInvitation');


    Route.resource('/empresa', 'gerencial/EmpresaController');
    Route.resource('/qrcodes', 'gerencial/QrCodeController');
    Route.resource('/relatorios/qrcode', 'relatorios/QrCodesRelatorioController');
    Route.resource('/setor', 'gerencial/SetorController');
    Route.resource('/usuario', 'gerencial/UsersController');
    Route.resource('/configuracao', 'gerencial/ConfiguracaoController');
    Route.resource('/estoque/categoria', 'estoque/CategoriaController');
    Route.resource('/estoque/produto', 'estoque/ProdutoController');
    Route.resource('/marketing/formulario', 'marketing/FormularioController');
    Route.resource('/marketing/formulario-items', 'marketing/FormularioItemController');
    Route.resource('/marketing/leads', 'marketing/LeadController');
    Route.resource('/marketing/leads-pastas', 'marketing/LeadsPastaController');
    Route.resource('/marketing/leads-etiquetas', 'marketing/EtiquetaController');
    Route.resource('/marketing/pagina-contato', 'marketing/PaginaContatoController');
    Route.resource('/modulos', 'gerencial/ModuloController');
    Route.resource('/gerencial/marcas', 'gerencial/MarcasModeloController');
    Route.resource('/gerencial/modelos', 'gerencial/ModeloController');
    Route.resource('/showroom/ofertas', 'showroom/OfertaController');
    Route.resource('/galeria-de-arquivos', 'gerencial/GaleriaDeArquivoController');
    Route.post('/galeria-de-arquivos/selected', 'gerencial/GaleriaDeArquivoController.getFilesSelecteds');
    Route.resource('/showroom/personalizar', 'showroom/PaginaShowRoomController');
    Route.resource('/showroom/banners', 'showroom/BannerController');


    Route.get('/gerencial/modelos_empresa', 'gerencial/ModeloController.modeloByEmpresa')
    Route.get('/empresa/gerenerate_qrcode/:id', 'gerencial/EmpresaController.qrCodeGenerate');
    Route.get('validar-url', 'gerencial/QrCodeController.urlChecker');
    Route.get('/verificar/email-usuario', 'gerencial/UsersController.checkEmailExists');
    Route.get('/marketing/leads-etiquetas-listar/:id', 'marketing/EtiquetaController.getByLead');
    Route.get('/marketing/add-etiqueta-lead/:lead_id/:etiqueta_id', 'marketing/EtiquetaController.addEtiquetaInLead');
    Route.get('/modulos-arvore', 'gerencial/ModuloController.modules');
    Route.get('/perfil', 'gerencial/UsersController.getProfile');
    Route.put('/perfil/:user_id', 'gerencial/UsersController.updateProfile');


}).middleware('auth').prefix('api');
