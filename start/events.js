const Event = use("Event");
const Encryption = use("Encryption");
const Database = use("Database");
const Mail = use("Mail");
const Env = use("Env");

const UsuarioEmpresaModel = use("App/Models/gerencial/UsuarioEmpresa");
const SetorModel = use("App/Models/gerencial/Setores");
const UsersModel = use("App/Models/User");
const PaginaContatoModel = use("App/Models/marketing/PaginaContato");
const FilaUsersMailModel = use("App/Models/core/FilaUsersEmail");
const EmpresaModel = use ("App/Models/gerencial/Empresa");

Event.on("new:empresaUsuarios", async (empresa, usuario) => {
  // Trigger para vincular todos os administradores ativos no sistema
  // a qualquer empresa criada
  let admins = await UsersModel.query()
                                  .where("nivel", "admin")
                                  .where("status", 1)
                                  .fetch();
  admins = admins.toJSON();

  const setor = await SetorModel.create({
    nome: "Gerencial",
    empresa_id: empresa.id,
  });

  // Criando as configurações de Layout Padrão
  // await PaginaContatoModel.create({
  //   custom_css: '{"main_color":"#2a93f1","secondary_color":"#d4d2d2","text_color":"#0e0606"}',
  //   configuracoes: '{"layout":"2r"}',
  //   titulo: empresa.nome,
  //   empresa_id: empresa.id
  // });

  console.log('Admins ->', admins);
  for (const item of admins) {
    var dados = {
      user_id: item.id,
      empresa_id: empresa.id,
      setor_id: setor.id,
    };
    await UsuarioEmpresaModel.create(dados);
  }

});

Event.on("new:sendUserInvite", async (user_id) => {
  const queryInvites = await FilaUsersMailModel.query().where('user_id', user_id).where('status', 0).first();
  const invite = queryInvites.toJSON();
  invite.user = await UsersModel.find(invite.user_id);
  invite.empresa = await EmpresaModel.find(invite.empresa_id);

  invite.empresa = invite.empresa.toJSON();
  invite.user = invite.user.toJSON();
  invite.link = `https://painel.fdvweb.com.br/#/cadastro?key=${encodeURIComponent(Encryption.encrypt(invite.user.id))}&ref=${encodeURIComponent(Encryption.encrypt(invite.empresa.id))}`;
  invite.json = JSON.stringify(invite);

  try {
    await Mail.send('emails/new-invite', { data: invite }, (message) => {
      message
        .to(invite.user.email)
        .from('contato@fdvweb.com.br', 'Convites | 🚀 Força de Vendas')
        .subject('Bem vindo ao Força de Vendas 🚀');
    });
    const inviteUpdate = await FilaUsersMailModel.find(invite.id);
    inviteUpdate.merge({ status: 1 });
    await inviteUpdate.save();
  } 
  catch (error) {
    const inviteUpdate = await FilaUsersMailModel.find(invite.id);
    inviteUpdate.merge({log: error,status: '-1'});
    await inviteUpdate.save();
  }
});
