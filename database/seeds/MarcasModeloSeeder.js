'use strict'
/*
|--------------------------------------------------------------------------
| MarcasModeloSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class MarcasModeloSeeder {
  async run() {
    const MarcasModel = use('App/Models/gerencial/MarcasModelos');

    await MarcasModel.createMany([
      {
        "nome": "AUDI",
        "fipe_name": "Audi",
        "fipe_order": 2,
        "fipe_key": "audi-6",
        "id": 6
      },
      {
        "nome": "BMW",
        "fipe_name": "BMW",
        "fipe_order": 2,
        "fipe_key": "bmw-7",
        "id": 7
      },
      {
        "nome": "CITROEN",
        "fipe_name": "Citroën",
        "fipe_order": 2,
        "fipe_key": "citroen-13",
        "id": 13
      },
      {
        "nome": "FIAT",
        "fipe_name": "Fiat",
        "fipe_order": 2,
        "fipe_key": "fiat-21",
        "id": 21
      },
      {
        "nome": "FORD",
        "fipe_name": "Ford",
        "fipe_order": 2,
        "fipe_key": "ford-22",
        "id": 22
      },
      {
        "nome": "CHEVROLET",
        "fipe_name": "GM - Chevrolet",
        "fipe_order": 2,
        "fipe_key": "gm-chevrolet-23",
        "id": 23
      },
      {
        "nome": "HONDA",
        "fipe_name": "Honda",
        "fipe_order": 2,
        "fipe_key": "honda-25",
        "id": 25
      },
      {
        "nome": "HYUNDAI",
        "fipe_name": "Hyundai",
        "fipe_order": 2,
        "fipe_key": "hyundai-26",
        "id": 26
      },
      {
        "nome": "KIA",
        "fipe_name": "Kia Motors",
        "fipe_order": 2,
        "fipe_key": "kia-motors-31",
        "id": 31
      },
      {
        "nome": "MERCEDES-BENZ",
        "fipe_name": "Mercedes-Benz",
        "fipe_order": 2,
        "fipe_key": "mercedes-benz-39",
        "id": 39
      },
      {
        "nome": "MITSUBISHI",
        "fipe_name": "Mitsubishi",
        "fipe_order": 2,
        "fipe_key": "mitsubishi-41",
        "id": 41
      },
      {
        "nome": "NISSAN",
        "fipe_name": "Nissan",
        "fipe_order": 2,
        "fipe_key": "nissan-43",
        "id": 43
      },
      {
        "nome": "PEUGEOT",
        "fipe_name": "Peugeot",
        "fipe_order": 2,
        "fipe_key": "peugeot-44",
        "id": 44
      },
      {
        "nome": "RENAULT",
        "fipe_name": "Renault",
        "fipe_order": 2,
        "fipe_key": "renault-48",
        "id": 48
      },
      {
        "nome": "SUZUKI",
        "fipe_name": "Suzuki",
        "fipe_order": 2,
        "fipe_key": "suzuki-55",
        "id": 55
      },
      {
        "nome": "TOYOTA",
        "fipe_name": "Toyota",
        "fipe_order": 2,
        "fipe_key": "toyota-56",
        "id": 56
      },
      {
        "nome": "VOLVO",
        "fipe_name": "Volvo",
        "fipe_order": 2,
        "fipe_key": "volvo-58",
        "id": 58
      },
      {
        "nome": "VOLKSWAGEN",
        "fipe_name": "VW - VolksWagen",
        "fipe_order": 2,
        "fipe_key": "vw-volkswagen-59",
        "id": 59
      },
      {
        "nome": "ACURA",
        "fipe_name": "Acura",
        "fipe_order": 1,
        "fipe_key": "acura-1",
        "id": 1
      },
      {
        "nome": "ALFA ROMEO",
        "fipe_name": "Alfa Romeo",
        "fipe_order": 1,
        "fipe_key": "alfa-romeo-3",
        "id": 3
      },
      {
        "nome": "AM GEN",
        "fipe_name": "AM Gen",
        "fipe_order": 1,
        "fipe_key": "am-gen-4",
        "id": 4
      },
      {
        "nome": "ASIA MOTORS",
        "fipe_name": "Asia Motors",
        "fipe_order": 1,
        "fipe_key": "asia-motors-5",
        "id": 5
      },
      {
        "nome": "ASTON MARTIN",
        "fipe_name": "ASTON MARTIN",
        "fipe_order": 1,
        "fipe_key": "aston-martin-189",
        "id": 189
      },
      {
        "nome": "BABY",
        "fipe_name": "Baby",
        "fipe_order": 1,
        "fipe_key": "baby-207",
        "id": 207
      },
      {
        "nome": "BRM",
        "fipe_name": "BRM",
        "fipe_order": 1,
        "fipe_key": "brm-8",
        "id": 8
      },
      {
        "nome": "BUGGY",
        "fipe_name": "Buggy",
        "fipe_order": 1,
        "fipe_key": "buggy-9",
        "id": 9
      },
      {
        "nome": "BUGRE",
        "fipe_name": "Bugre",
        "fipe_order": 1,
        "fipe_key": "bugre-123",
        "id": 123
      },
      {
        "nome": "CADILLAC",
        "fipe_name": "Cadillac",
        "fipe_order": 1,
        "fipe_key": "cadillac-10",
        "id": 10
      },
      {
        "nome": "CBT JIPE",
        "fipe_name": "CBT Jipe",
        "fipe_order": 1,
        "fipe_key": "cbt-jipe-11",
        "id": 11
      },
      {
        "nome": "CHANA",
        "fipe_name": "CHANA",
        "fipe_order": 1,
        "fipe_key": "chana-136",
        "id": 136
      },
      {
        "nome": "CHANGAN",
        "fipe_name": "CHANGAN",
        "fipe_order": 1,
        "fipe_key": "changan-182",
        "id": 182
      },
      {
        "nome": "CHERY",
        "fipe_name": "CHERY",
        "fipe_order": 1,
        "fipe_key": "chery-161",
        "id": 161
      },
      {
        "nome": "CHRYSLER",
        "fipe_name": "Chrysler",
        "fipe_order": 1,
        "fipe_key": "chrysler-12",
        "id": 12
      },
      {
        "nome": "CROSS LANDER",
        "fipe_name": "Cross Lander",
        "fipe_order": 1,
        "fipe_key": "cross-lander-14",
        "id": 14
      },
      {
        "nome": "DAEWOO",
        "fipe_name": "Daewoo",
        "fipe_order": 1,
        "fipe_key": "daewoo-15",
        "id": 15
      },
      {
        "nome": "DAIHATSU",
        "fipe_name": "Daihatsu",
        "fipe_order": 1,
        "fipe_key": "daihatsu-16",
        "id": 16
      },
      {
        "nome": "DODGE",
        "fipe_name": "Dodge",
        "fipe_order": 1,
        "fipe_key": "dodge-17",
        "id": 17
      },
      {
        "nome": "EFFA",
        "fipe_name": "EFFA",
        "fipe_order": 1,
        "fipe_key": "effa-147",
        "id": 147
      },
      {
        "nome": "ENGESA",
        "fipe_name": "Engesa",
        "fipe_order": 1,
        "fipe_key": "engesa-18",
        "id": 18
      },
      {
        "nome": "ENVEMO",
        "fipe_name": "Envemo",
        "fipe_order": 1,
        "fipe_key": "envemo-19",
        "id": 19
      },
      {
        "nome": "FERRARI",
        "fipe_name": "Ferrari",
        "fipe_order": 1,
        "fipe_key": "ferrari-20",
        "id": 20
      },
      {
        "nome": "FIBRAVAN",
        "fipe_name": "Fibravan",
        "fipe_order": 1,
        "fipe_key": "fibravan-149",
        "id": 149
      },
      {
        "nome": "FOTON",
        "fipe_name": "FOTON",
        "fipe_order": 1,
        "fipe_key": "foton-190",
        "id": 190
      },
      {
        "nome": "FYBER",
        "fipe_name": "Fyber",
        "fipe_order": 1,
        "fipe_key": "fyber-170",
        "id": 170
      },
      {
        "nome": "GEELY",
        "fipe_name": "GEELY",
        "fipe_order": 1,
        "fipe_key": "geely-199",
        "id": 199
      },
      {
        "nome": "GREAT WALL",
        "fipe_name": "GREAT WALL",
        "fipe_order": 1,
        "fipe_key": "great-wall-153",
        "id": 153
      },
      {
        "nome": "GURGEL",
        "fipe_name": "Gurgel",
        "fipe_order": 1,
        "fipe_key": "gurgel-24",
        "id": 24
      },
      {
        "nome": "HAFEI",
        "fipe_name": "HAFEI",
        "fipe_order": 1,
        "fipe_key": "hafei-152",
        "id": 152
      },
      {
        "nome": "HITECH ELECTRIC",
        "fipe_name": "HITECH ELECTRIC",
        "fipe_order": 1,
        "fipe_key": "hitech-electric-214",
        "id": 214
      },
      {
        "nome": "HITECH ELETRIC",
        "fipe_name": "HITECH ELETRIC",
        "fipe_order": 1,
        "fipe_key": "hitech-eletric-213",
        "id": 213
      },
      {
        "nome": "ISUZU",
        "fipe_name": "Isuzu",
        "fipe_order": 1,
        "fipe_key": "isuzu-27",
        "id": 27
      },
      {
        "nome": "IVECO",
        "fipe_name": "IVECO",
        "fipe_order": 1,
        "fipe_key": "iveco-208",
        "id": 208
      },
      {
        "nome": "JAC",
        "fipe_name": "JAC",
        "fipe_order": 1,
        "fipe_key": "jac-177",
        "id": 177
      },
      {
        "nome": "JAGUAR",
        "fipe_name": "Jaguar",
        "fipe_order": 1,
        "fipe_key": "jaguar-28",
        "id": 28
      },
      {
        "nome": "JEEP",
        "fipe_name": "Jeep",
        "fipe_order": 1,
        "fipe_key": "jeep-29",
        "id": 29
      },
      {
        "nome": "JINBEI",
        "fipe_name": "JINBEI",
        "fipe_order": 1,
        "fipe_key": "jinbei-154",
        "id": 154
      },
      {
        "nome": "JPX",
        "fipe_name": "JPX",
        "fipe_order": 1,
        "fipe_key": "jpx-30",
        "id": 30
      },
      {
        "nome": "LADA",
        "fipe_name": "Lada",
        "fipe_order": 1,
        "fipe_key": "lada-32",
        "id": 32
      },
      {
        "nome": "LAMBORGHINI",
        "fipe_name": "LAMBORGHINI",
        "fipe_order": 1,
        "fipe_key": "lamborghini-171",
        "id": 171
      },
      {
        "nome": "LAND ROVER",
        "fipe_name": "Land Rover",
        "fipe_order": 1,
        "fipe_key": "land-rover-33",
        "id": 33
      },
      {
        "nome": "LEXUS",
        "fipe_name": "Lexus",
        "fipe_order": 1,
        "fipe_key": "lexus-34",
        "id": 34
      },
      {
        "nome": "LIFAN",
        "fipe_name": "LIFAN",
        "fipe_order": 1,
        "fipe_key": "lifan-168",
        "id": 168
      },
      {
        "nome": "LOBINI",
        "fipe_name": "LOBINI",
        "fipe_order": 1,
        "fipe_key": "lobini-127",
        "id": 127
      },
      {
        "nome": "LOTUS",
        "fipe_name": "Lotus",
        "fipe_order": 1,
        "fipe_key": "lotus-35",
        "id": 35
      },
      {
        "nome": "MAHINDRA",
        "fipe_name": "Mahindra",
        "fipe_order": 1,
        "fipe_key": "mahindra-140",
        "id": 140
      },
      {
        "nome": "MASERATI",
        "fipe_name": "Maserati",
        "fipe_order": 1,
        "fipe_key": "maserati-36",
        "id": 36
      },
      {
        "nome": "MATRA",
        "fipe_name": "Matra",
        "fipe_order": 1,
        "fipe_key": "matra-37",
        "id": 37
      },
      {
        "nome": "MAZDA",
        "fipe_name": "Mazda",
        "fipe_order": 1,
        "fipe_key": "mazda-38",
        "id": 38
      },
      {
        "nome": "MCLAREN",
        "fipe_name": "Mclaren",
        "fipe_order": 1,
        "fipe_key": "mclaren-211",
        "id": 211
      },
      {
        "nome": "MERCURY",
        "fipe_name": "Mercury",
        "fipe_order": 1,
        "fipe_key": "mercury-40",
        "id": 40
      },
      {
        "nome": "MG",
        "fipe_name": "MG",
        "fipe_order": 1,
        "fipe_key": "mg-167",
        "id": 167
      },
      {
        "nome": "MINI",
        "fipe_name": "MINI",
        "fipe_order": 1,
        "fipe_key": "mini-156",
        "id": 156
      },
      {
        "nome": "MIURA",
        "fipe_name": "Miura",
        "fipe_order": 1,
        "fipe_key": "miura-42",
        "id": 42
      },
      {
        "nome": "PLYMOUTH",
        "fipe_name": "Plymouth",
        "fipe_order": 1,
        "fipe_key": "plymouth-45",
        "id": 45
      },
      {
        "nome": "PONTIAC",
        "fipe_name": "Pontiac",
        "fipe_order": 1,
        "fipe_key": "pontiac-46",
        "id": 46
      },
      {
        "nome": "PORSCHE",
        "fipe_name": "Porsche",
        "fipe_order": 1,
        "fipe_key": "porsche-47",
        "id": 47
      },
      {
        "nome": "RAM",
        "fipe_name": "RAM",
        "fipe_order": 1,
        "fipe_key": "ram-185",
        "id": 185
      },
      {
        "nome": "RELY",
        "fipe_name": "RELY",
        "fipe_order": 1,
        "fipe_key": "rely-186",
        "id": 186
      },
      {
        "nome": "ROLLS-ROYCE",
        "fipe_name": "Rolls-Royce",
        "fipe_order": 1,
        "fipe_key": "rolls-royce-195",
        "id": 195
      },
      {
        "nome": "ROVER",
        "fipe_name": "Rover",
        "fipe_order": 1,
        "fipe_key": "rover-49",
        "id": 49
      },
      {
        "nome": "SAAB",
        "fipe_name": "Saab",
        "fipe_order": 1,
        "fipe_key": "saab-50",
        "id": 50
      },
      {
        "nome": "SATURN",
        "fipe_name": "Saturn",
        "fipe_order": 1,
        "fipe_key": "saturn-51",
        "id": 51
      },
      {
        "nome": "SEAT",
        "fipe_name": "Seat",
        "fipe_order": 1,
        "fipe_key": "seat-52",
        "id": 52
      },
      {
        "nome": "SHINERAY",
        "fipe_name": "SHINERAY",
        "fipe_order": 1,
        "fipe_key": "shineray-183",
        "id": 183
      },
      {
        "nome": "SMART",
        "fipe_name": "smart",
        "fipe_order": 1,
        "fipe_key": "smart-157",
        "id": 157
      },
      {
        "nome": "SSANGYONG",
        "fipe_name": "SSANGYONG",
        "fipe_order": 1,
        "fipe_key": "ssangyong-125",
        "id": 125
      },
      {
        "nome": "SUBARU",
        "fipe_name": "Subaru",
        "fipe_order": 1,
        "fipe_key": "subaru-54",
        "id": 54
      },
      {
        "nome": "TAC",
        "fipe_name": "TAC",
        "fipe_order": 1,
        "fipe_key": "tac-165",
        "id": 165
      },
      {
        "nome": "TROLLER",
        "fipe_name": "Troller",
        "fipe_order": 1,
        "fipe_key": "troller-57",
        "id": 57
      },
      {
        "nome": "WAKE",
        "fipe_name": "Wake",
        "fipe_order": 1,
        "fipe_key": "wake-163",
        "id": 163
      },
      {
        "nome": "WALK",
        "fipe_name": "Walk",
        "fipe_order": 1,
        "fipe_key": "walk-120",
        "id": 120
      }
    ])


  }
}

module.exports = MarcasModeloSeeder
