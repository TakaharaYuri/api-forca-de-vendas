'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeminovosCaracteristicasDadosSchema extends Schema {
  up() {
    this.create('seminovos_caracteristicas_dados', (table) => {
      table.increments()
      table.timestamps()
      table
        .integer('seminovo_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('seminovos')
        .onUpdate('CASCADE')
      table
        .integer('caracteristica_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('seminovos_caracteristicas')
        .onUpdate('CASCADE')
    })
  }

  down() {
    this.drop('seminovos_caracteristicas_dados')
  }
}

module.exports = SeminovosCaracteristicasDadosSchema
