'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EtiquetasLeadsSchema extends Schema {
  up () {
    this.create('etiquetas_leads', (table) => {
      table.increments()
      table.timestamps()
      table
        .integer("etiqueta_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("etiquetas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE")
      table
        .integer("lead_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("leads")
        .onUpdate("CASCADE")
        .onDelete("CASCADE")
      
    })
  }

  down () {
    this.drop('etiquetas_leads')
  }
}

module.exports = EtiquetasLeadsSchema
