'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QrCodesRelatoriosSchema extends Schema {
  up () {
    this.table('qr_codes_relatorios', (table) => {
      // alter table
      table.string('city');
      table.string('region');
      table.string('country');
      table.dropColumn('urm_source');
      table.string('utm_source');
    })
  }

  down () {
    this.table('qr_codes_relatorios', (table) => {
      // reverse alternations
      table.dropColumn('city');
      table.dropColumn('region');
      table.dropColumn('country');
    })
  }
}

module.exports = QrCodesRelatoriosSchema
