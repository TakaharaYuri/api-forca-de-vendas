'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeadsSchema extends Schema {
  up () {
    this.table('leads', (table) => {
      // alter table
      table.string('intencao_retirada');
      table.string('prazo_ideal');
      table.string('valor_parcelas');
      table.decimal('valor_lance',[10,2]);
    })
  }

  down () {
    this.table('leads', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LeadsSchema
