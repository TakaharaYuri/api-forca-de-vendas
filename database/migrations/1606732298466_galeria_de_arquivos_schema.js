'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GaleriaDeArquivosSchema extends Schema {
  up () {
    this.table('galeria_de_arquivos', (table) => {
      // alter table
      table.integer('altura');
      table.integer('largura');
    })
  }

  down () {
    this.table('galeria_de_arquivos', (table) => {
      // reverse alternations
      table.dropColumn('altura');
      table.dropColumn('largura');
    })
  }
}

module.exports = GaleriaDeArquivosSchema
