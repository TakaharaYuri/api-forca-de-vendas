'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeminovosCaracteristicasSchema extends Schema {
  up () {
    this.create('seminovos_caracteristicas', (table) => {
      table.increments()
      table.timestamps()
      table.string('especie').defaultTo('automovel')
      table.string('nome')
      table.boolean('status').defaultTo(true)      
      table.string('tipo').defaultTo('caracteristica')
    })
  }

  down () {
    this.drop('seminovos_caracteristicas')
  }
}

module.exports = SeminovosCaracteristicasSchema
