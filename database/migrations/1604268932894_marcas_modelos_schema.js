'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MarcasModelosSchema extends Schema {
  up () {
    this.create('marcas_modelos', (table) => {
      table.increments();
      table.timestamps();
      table.string('nome');
      table.string('imagem');
      table.boolean('status').defaultTo(true);
      table.json('meta_dados');
    })
  }

  down () {
    this.drop('marcas_modelos')
  }
}

module.exports = MarcasModelosSchema
