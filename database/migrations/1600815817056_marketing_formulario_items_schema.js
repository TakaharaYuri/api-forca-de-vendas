'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormularioItemsSchema extends Schema {
  up () {
    this.table('formulario_items', (table) => {
      // alter table
      table.string('identifier').defaultTo(null);
    })
  }

  down () {
    this.table('formulario_items', (table) => {
      table.dropColumn('identifier');
    })
  }
}

module.exports = FormularioItemsSchema
