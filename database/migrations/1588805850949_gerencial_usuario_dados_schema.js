"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UsuarioDadosSchema extends Schema {
  up() {
    this.create("usuario_dados", (table) => {
      table.string("nome");
      table.datetime("data_nascimento");
      table.string("imagem");
      table.string("apelido");
      table.string("instagram");
      table.string("celular");
      table.string('cpf');
      table
        .integer("user_id")
        .unsigned()
        .references("id")
        .inTable("users")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.increments();
      table.timestamps();
    });
  }

  down() {
    this.drop("usuario_dados");
  }
}

module.exports = UsuarioDadosSchema;
