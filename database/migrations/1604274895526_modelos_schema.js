'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ModelosSchema extends Schema {
  up () {
    this.create('modelos', (table) => {
      table.increments()
      table.timestamps()
      table.string('nome');
      table.string('versao');
      table.string('motorizacao');
      table.string('ano');
      table.decimal('valor', [10,2]);
      table.string('carroceria');
      table.json('imagens');
      table.json('meta_dados');
      table.boolean('status').defaultTo(true);
      table
        .integer('marca_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('marcas_modelos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down () {
    this.drop('modelos')
  }
}

module.exports = ModelosSchema
