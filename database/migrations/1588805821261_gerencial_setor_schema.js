"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SetorSchema extends Schema {
  up() {
    this.create("setores", (table) => {
      table.string("nome");
      table.boolean("status").defaultTo(true);
      table.text("meta_dados");
      table
        .integer("empresa_id")
        .unsigned()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.increments();
      table.timestamps();
    });
  }

  down() {
    this.drop("setores");
  }
}

module.exports = SetorSchema;
