'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MarcasModelosSchema extends Schema {
  up () {
    this.table('marcas_modelos', (table) => {
      // alter table
      table.string('fipe_name')
      table.string('fipe_key')
      table.string('fipe_order')
    })
  }

  down () {
    this.table('marcas_modelos', (table) => {
      // reverse alternations
      table.dropColumn('fipe_name')
      table.dropColumn('fipe_key')
      table.dropColumn('fipe_order')
    })
  }
}

module.exports = MarcasModelosSchema
