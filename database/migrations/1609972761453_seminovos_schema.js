'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeminovosSchema extends Schema {
  up() {
    this.table('seminovos', (table) => {
      table.string('ano')
      table.string('ano_modelo')
      table.string('chassi')
      table.string('codigo_situacao')
      table.string('sitacao')
      table.string('cor')
      table.datetime('data_consulta')
      table.date('data_ultima_atualizacao')
      table.string('nome')
      table.string('placa')
      table.string('uf')
      table.string('municipio')
      table.string('localizacao_atual')
      table.json('imagens')
      table.json('documentos')
      table.integer('tipo')
      table.boolean('status').defaultTo(1)
      table.string('especie').defaultTo('automovel')
      table.string('combustivel')
      table.string('renavam')
      table.string('numero_motor')
      table.string('quilometragem')
      table.string('motorizacao')
      table.string('cambio')
      table.decimal('valor_fipe', [10, 2])
      table.decimal('valor_venda', [10, 2])
      table.decimal('valor_compra', [10, 2])
      table.uuid('uid')
      table.string('url')
      table.string('hp')
      table.text('descricao')
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
      table
        .integer('marca_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('marcas_modelos')
        .onUpdate('CASCADE')
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
      // alter table
    })
  }

  down() {
    this.table('seminovos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SeminovosSchema
