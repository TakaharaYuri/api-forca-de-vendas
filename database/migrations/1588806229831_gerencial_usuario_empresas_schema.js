"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UsuarioEmpresasSchema extends Schema {
  up() {
    this.create("usuario_empresas", (table) => {
      table.string("nivel");
      table.boolean("status").defaultTo(true);
      table.integer("posicao_fila").defaultTo(0);
      table.integer("posicao_vez").defaultTo(0);
      table.boolean("ativo_fila").defaultTo(true);
      table
        .integer("user_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("users")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("setor_id")
        .unsigned()
        .references("id")
        .inTable("setores")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.increments();
      table.timestamps();
    });
  }

  down() {
    this.drop("usuario_empresas");
  }
}

module.exports = UsuarioEmpresasSchema;
