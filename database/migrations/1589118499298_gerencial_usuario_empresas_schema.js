'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsuarioEmpresasSchema extends Schema {
  up () {
    // this.table('usuario_empresas', (table) => {
    //   // alter table
    // })
    this.alter('usuario_empresas', (table) => {
      table.text('permissoes');
    });
  }

  down () {
    this.table('usuario_empresas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UsuarioEmpresasSchema
