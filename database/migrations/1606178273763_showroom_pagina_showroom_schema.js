'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaginaShowroomSchema extends Schema {
  up () {
    this.create('pagina_showrooms', (table) => {
      table.increments()
      table.timestamps()
      table.json('layout')
      table.json('seo')
      table.json('configuracoes')
      table.json('meta_dados')
      table.boolean('status').defaultTo(true)
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
    })
  }

  down () {
    this.drop('pagina_showrooms')
  }
}

module.exports = PaginaShowroomSchema
