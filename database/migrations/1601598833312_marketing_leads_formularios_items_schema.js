'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeadsFormulariosItensSchema extends Schema {
  up() {
    this.create('leads_formularios_items', (table) => {
      table.increments()
      table.timestamps()
      table
        .integer("lead_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("leads")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("formulario_item_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("formulario_items")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.text('valor')
    })

  }


  down() {
    this.drop('leads_formularios_itens')
  }
}


module.exports = LeadsFormulariosItensSchema
