'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaginaContatoSchema extends Schema {
  up () {
    this.table('pagina_contato', (table) => {
      table.text('custom_scripts');
    })
  }

  down () {
    this.table('pagina_contato', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PaginaContatoSchema
