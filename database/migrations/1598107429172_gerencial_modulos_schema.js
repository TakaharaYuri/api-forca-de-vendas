'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ModulosSchema extends Schema {
  up() {
    this.create('modulos', (table) => {
      table.increments();
      table.timestamps();
      table.string('nome');
      table.string('descricao');
      table.string('icone');
      table.integer('status').defaultTo(1);
      table.string('url');
      table.text('meta_dados');
      table.integer('id_grupo');
      table.integer('posicao');
      table.string('tag');
      table.string('tipo').defaultTo('geral');
    })
  }

  down() {
    this.drop('modulos')
  }
}

module.exports = ModulosSchema
