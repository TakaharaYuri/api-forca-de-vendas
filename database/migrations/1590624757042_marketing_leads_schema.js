"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class LeadsSchema extends Schema {
  up() {
    this.create("leads", (table) => {
      table.increments();
      table.timestamps();
      table.string("tipo");
      table.string("origem");
      table.integer("status").defaultTo(1);
      table.text("mensagem");
      table.text("meta_dados");
      table.string("metodo_pagamento");
      table.text("headers");
      table.decimal("valor_entrada", [10, 2]);
      table.text("raw");
      table
        .integer("contato_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("contatos")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("user_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("users")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("produto_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("produtos")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down() {
    this.drop("leads");
  }
}

module.exports = LeadsSchema;
