'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QrCodesSchema extends Schema {
  up() {
    this.create('qr_codes', (table) => {
      table.increments()
      table.timestamps()
      table.string('nome')
      table.string('qrcode')
      table.string('link')
      table.boolean('status').defaultTo(true)
      table.text('meta_dados')
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    })
  }

  down() {
    this.drop('qr_codes')
  }
}

module.exports = QrCodesSchema
