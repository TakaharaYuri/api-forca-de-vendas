"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class IntegracoesSchema extends Schema {
  up() {
    this.create("integracoes", (table) => {
      table.increments();
      table.timestamps();
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.string('nome');
      table.string('descricao');
      table.integer('status').defaultTo(1);
      table.text('meta_dados');
    });
  }

  down() {
    this.drop("integracoes");
  }
}

module.exports = IntegracoesSchema;
