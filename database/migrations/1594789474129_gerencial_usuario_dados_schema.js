'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsuarioDadosSchema extends Schema {
  up () {
    this.table('usuario_dados', (table) => {
      // alter table
      table.string('facebook');
      table.string('sexo');
    })
  }

  down () {
    this.table('usuario_dados', (table) => {
      // reverse alternations
      table.dropColumn('facebook');
      table.dropColumn('sexo');
    })
  }
}

module.exports = UsuarioDadosSchema
