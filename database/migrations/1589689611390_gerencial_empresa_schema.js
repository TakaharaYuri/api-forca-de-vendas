'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresaSchema extends Schema {
  up () {
    this.alter('empresas', (table) => {
      table.text('qrcode_reference');
    })
  }

  down () {
    this.drop('empresas')
  }
}

module.exports = EmpresaSchema
