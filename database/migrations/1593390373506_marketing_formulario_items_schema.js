'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormularioItemsSchema extends Schema {
  up () {
    this.create('formulario_items', (table) => {
      table.increments()
      table.timestamps();
      table.string('name');
      table.boolean('enabled').defaultTo(true);
      table.boolean('required').defaultTo(false);
      table.string('type');
      table.text('options');
      table.text('selected_options');
      table.text('conditions');
      table.text('raw_data');
      table
        .integer("formulario_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("formularios")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down () {
    this.drop('formulario_items')
  }
}

module.exports = FormularioItemsSchema
