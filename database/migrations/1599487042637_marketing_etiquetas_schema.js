'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EtiquetasSchema extends Schema {
  up () {
    this.create('etiquetas', (table) => {
      table.increments()
      table.timestamps()
      table.string('nome')
      table.string('cor').defaultTo('#109ddd')
      table.string('icone').defaultTo(null)
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE")
    })
  }

  down () {
    this.drop('etiquetas')
  }
}

module.exports = EtiquetasSchema
