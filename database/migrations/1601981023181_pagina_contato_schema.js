'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaginaContatoSchema extends Schema {
  up() {
    this.create('pagina_contato', (table) => {
      table.increments()
      table.timestamps()
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.string('titulo');
      table.string('descricao')
      table.json('layout')
      table.json('seo')
      table.json('configuracoes')
      table.text('aviso_legal')
    })
  }

  down() {
    this.drop('pagina_contato')
  }
}

module.exports = PaginaContatoSchema
