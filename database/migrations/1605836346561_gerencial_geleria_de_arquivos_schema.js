'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GaleriaDeArquivosSchema extends Schema {
  up () {
    this.create('galeria_de_arquivos', (table) => {
      table.increments()
      table.timestamps()
      table.string('url')
      table.json('meta_dados')
      table.integer('tamanho')
      table.string('nome')
      table.text('descricao')
      table.string('formato')
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down () {
    this.drop('galeria_de_arquivos')
  }
}

module.exports = GaleriaDeArquivosSchema
