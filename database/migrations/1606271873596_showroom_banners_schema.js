'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BannersSchema extends Schema {
  up () {
    this.create('banners', (table) => {
      table.increments()
      table.timestamps()
      table.string('titulo')
      table.string('link')
      table.boolean('status').defaultTo(true)
      table.string('imagem_desktop')
      table.string('imagem_mobile')
      table.json('meta_dados')
      table.timestamp('data_inicio').defaultTo(null)
      table.timestamp('data_fim').defaultTo(null)
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
    })
  }

  down () {
    this.drop('banners')
  }
}

module.exports = BannersSchema
