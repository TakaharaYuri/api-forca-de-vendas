'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormularioItemsSchema extends Schema {
  up () {
    this.table('formulario_items', (table) => {
      table.integer('position_index');
    })
  }

  down () {
    this.table('formulario_items', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FormularioItemsSchema
