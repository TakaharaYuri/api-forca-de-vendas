"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class PaginaContatoSchema extends Schema {
  up() {
    this.create("pagina_contato", (table) => {
      table.increments();
      table.timestamps();
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.string("titulo");
      table.string("descricao");
      table.string("icone");
      table.boolean("status").defaultTo(true);
      table.string("keywords");
      table.text("meta_dados");
      table.text("custom_css");
    });
  }

  down() {
    this.drop("pagina_contato");
  }
}

module.exports = PaginaContatoSchema;
