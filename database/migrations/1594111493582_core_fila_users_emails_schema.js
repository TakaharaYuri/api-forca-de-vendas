'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FilaUsersEmailsSchema extends Schema {
  up() {
    this.create('fila_users_emails', (table) => {
      table.increments();
      table.timestamps();
      table.string('tipo');
      table.string('modelo');
      table.integer('status').defaultTo(0);
      table.text('log');
      table
        .integer("user_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("users")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    })
  }

  down() {
    this.drop('fila_users_emails')
  }
}

module.exports = FilaUsersEmailsSchema
