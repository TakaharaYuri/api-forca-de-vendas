'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeadsSchema extends Schema {
  up () {
    this.table('leads', (table) => {
      table
        .integer("id_pasta")
        .defaultTo(null)
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("leads_pastas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE")
    })
  }

  down () {
    this.table('leads', (table) => {
      table.dropColumn('id_pasta');
    })
  }
}

module.exports = LeadsSchema
