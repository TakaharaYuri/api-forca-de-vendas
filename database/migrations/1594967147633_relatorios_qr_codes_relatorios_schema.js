'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QrCodesRelatoriosSchema extends Schema {
  up() {
    this.create('qr_codes_relatorios', (table) => {
      table.increments()
      table.timestamps()
      table.string('urm_source')
      table.string('utm_campaign')
      table.string('utm_medium')
      table.string('utm_term')
      table.string('utm_origin')
      table.string('ip')
      table.string('url')
      table.string('device')
      table.text('device_raw')
      table.text('request_raw')
      table.timestamp('permanence_time')
      table.text('headers')
      table.string('navigator')
      table.string('so')
      table.text('user_agent')
      table.text('hash')
      table
        .integer("qrcode_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("qr_codes")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
  })
}

down() {
  this.drop('qr_codes_relatorios')
}
}

module.exports = QrCodesRelatoriosSchema
