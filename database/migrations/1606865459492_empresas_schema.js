'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresasSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      table
        .integer('marca_id')
        .unsigned()
        .references('id')
        .inTable('marcas_modelos')
        .onUpdate('CASCADE')
    })
  }

  down () {
    this.table('empresas', (table) => {
      table.dropColumn('marcas_modelos');
    })
  }
}

module.exports = EmpresasSchema
