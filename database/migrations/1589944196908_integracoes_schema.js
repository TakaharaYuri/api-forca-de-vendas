'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IntegracoesSchema extends Schema {
  up () {
    this.drop("integracoes");
  }

  down () {
    this.table('integracoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = IntegracoesSchema
