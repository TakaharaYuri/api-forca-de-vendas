'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ConfiguracaoSchema extends Schema {
  up () {
    this.create('configuracaos', (table) => {
      table.string('smtp_nome');
      table.string('smtp_host');
      table.string('smtp_user');
      table.string('smtp_password');
      table.string('smtp_port');
      table.string('smtp_type');
      table.string('smtp_crypto');
      table.string('smtp_newline');
      table.string('smtp_charset');
      table.string('smtp_sender');
      table.string('main_url');
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('configuracaos')
  }
}

module.exports = ConfiguracaoSchema
