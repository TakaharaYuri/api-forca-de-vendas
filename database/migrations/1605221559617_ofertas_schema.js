'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OfertasSchema extends Schema {
  up () {
    this.table('ofertas', (table) => {
      // alter table
      table.string('url').defaultTo(null);
    })
  }

  down () {
    this.table('ofertas', (table) => {
      // reverse alternations
      table.dropColumn('url')
    })
  }
}

module.exports = OfertasSchema
