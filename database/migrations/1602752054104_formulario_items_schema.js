'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormularioItemsSchema extends Schema {
  up () {
    this.table('formulario_items', (table) => {
      // alter table
      table.integer('parent_id')
    })
  }

  down () {
    this.table('formulario_items', (table) => {
      table.dropColumn('parent_id')
    })
  }
}

module.exports = FormularioItemsSchema
