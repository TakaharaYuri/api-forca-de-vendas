'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QrCodesSchema extends Schema {
  up () {
    this.table('qr_codes', (table) => {
      table.string('tipo');
    })
  }

  down () {
    this.table('qr_codes', (table) => {
      table.dropColumn('tipo');
    })
  }
}

module.exports = QrCodesSchema
