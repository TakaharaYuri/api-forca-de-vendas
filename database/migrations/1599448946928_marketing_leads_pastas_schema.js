'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeadsPastasSchema extends Schema {
  up () {
    this.create('leads_pastas', (table) => {
      table.increments()
      table.timestamps()
      table.string('nome')
      table.boolean('status').defaultTo(true)
      table.text('descricao')
      table.text('meta_dados')
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE")
    })
  }

  down () {
    this.drop('leads_pastas')
  }
}

module.exports = LeadsPastasSchema
