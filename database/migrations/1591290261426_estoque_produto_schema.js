'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      table.string('imagem').defaultTo(null);
    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutoSchema
