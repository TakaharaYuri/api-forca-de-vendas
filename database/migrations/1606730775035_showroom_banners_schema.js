'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BannersSchema extends Schema {
  up () {
    this.drop('banners');
  }

  down () {
    this.table('banners', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BannersSchema
