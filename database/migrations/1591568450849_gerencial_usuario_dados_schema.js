'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsuarioDadosSchema extends Schema {
  up () {
    this.table('usuario_dados', (table) => {
      table.string('syonet_usuario');
      table.string('syonet_senha');
      table.text('syonet_configuracoes');
    })
  }

  down () {
    this.table('usuario_dados', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UsuarioDadosSchema
