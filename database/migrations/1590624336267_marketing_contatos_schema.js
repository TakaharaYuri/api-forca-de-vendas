"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ContatosSchema extends Schema {
  up() {
    this.create("contatos", (table) => {
      table.increments();
      table.timestamps();
      table.string("nome");
      table.string("email");
      table.string("celular");
      table.boolean("whatsapp_ativo");
      table.integer("status").defaultTo(1);
      table.string("ip");
      table.string("imagem");
      table.string("cpf");
      table.string("cnpj");
      table.datetime("data_nascimento");
      table.integer("tipo_pessoa").defaultTo(1);
      table.string("uf");
      table.string("cidade");
      table.string("bairro");
      table.string("endereco");
      table.string("complemento");
      table.string("numero");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down() {
    this.drop("contatos");
  }
}

module.exports = ContatosSchema;
