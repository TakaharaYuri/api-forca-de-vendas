'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaginaContatoSchema extends Schema {
  up() {
    this.table('pagina_contato', (table) => {
      table
        .integer('formulario_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('formularios')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down() {
    this.table('pagina_contato', (table) => {
      table.dropColumn('formulario_id');
    })
  }
}

module.exports = PaginaContatoSchema
