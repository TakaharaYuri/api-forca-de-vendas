'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermissoesSchema extends Schema {
  up() {
    this.create('permissoes', (table) => {
      table.increments();
      table.timestamps();
      table.integer("id_setor")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("setores")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.integer("id_modulo")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("modulos")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.text('meta_dados');
    })
  }

  down() {
    this.drop('permissoes')
  }
}

module.exports = PermissoesSchema
