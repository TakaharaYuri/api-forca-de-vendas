"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ProdutoSchema extends Schema {
  up() {
    this.create("produtos", (table) => {
      table.increments();
      table.timestamps();
      table.string("nome");
      table.integer("tipo");
      table.boolean("ativo").defaultTo(true);
      table
        .integer("categoria_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("categorias")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down() {
    this.drop("produtos");
  }
}

module.exports = ProdutoSchema;
