'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresasSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      // alter table
      table.string('host')
    })
  }

  down () {
    this.table('empresas', (table) => {
      table.dropColumn('host')
    })
  }
}

module.exports = EmpresasSchema
