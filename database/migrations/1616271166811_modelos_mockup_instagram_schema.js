'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ModelosMockupInstagramSchema extends Schema {
  up () {
    this.create('modelos_mockup_instagram', (table) => {
      table.increments()
      table.timestamps()
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
      table.string('nome')
      table.boolean('status').defaultTo(true);
      table.json('configuracoes')
      table.string('background')
    })
  }

  down () {
    this.drop('modelos_mockup_instagram')
  }
}

module.exports = ModelosMockupInstagramSchema
