'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaginaContatoSchema extends Schema {
  up () {
    this.drop('pagina_contato')
  }

  down () {
    this.table('pagina_contato', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PaginaContatoSchema
