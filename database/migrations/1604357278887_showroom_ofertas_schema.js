'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OfertasSchema extends Schema {
  up() {
    this.create('ofertas', (table) => {
      table.increments()
      table.timestamps()
      table.string('tipo')
      table.string('nome')
      table.string('complemento')
      table.boolean('status').defaultTo(true)
      table.boolean('destacar').defaultTo(true)
      table.json('selos')
      table.text('condicoes')
      table.timestamp('data_inicio')
      table.timestamp('data_fim')
      table.json('meta_dados')
      table
        .integer('empresa_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('empresas')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table
        .integer('modelo_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('modelos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down() {
    this.drop('ofertas')
  }
}

module.exports = OfertasSchema
