"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CategoriasSchema extends Schema {
  up() {
    this.create("categorias", (table) => {
      table.increments();
      table.timestamps();
      table.string("nome");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down() {
    this.drop("categorias");
  }
}

module.exports = CategoriasSchema;
