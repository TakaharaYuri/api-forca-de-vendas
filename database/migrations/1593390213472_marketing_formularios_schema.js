'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormulariosSchema extends Schema {
  up () {
    this.create('formularios', (table) => {
      table.increments();
      table.timestamps();
      table.string("nome");
      table.text("descricao");
      table.string("imagem");
      table.boolean("status").defaultTo(false);
      table.text("configuracoes");
      table
        .integer("empresa_id")
        .unsigned()
        .notNullable()
        .references("id")
        .inTable("empresas")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down () {
    this.drop('formularios')
  }
}

module.exports = FormulariosSchema
