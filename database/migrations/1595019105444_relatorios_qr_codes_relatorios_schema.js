'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QrCodesRelatoriosSchema extends Schema {
  up () {
    this.table('qr_codes_relatorios', (table) => {
      // alter table
      table.string('reference_id')
    })
  }

  down () {
    this.table('qr_codes_relatorios', (table) => {
      // reverse alternations
      table.dropColumn('reference_id')
    })
  }
}

module.exports = QrCodesRelatoriosSchema
