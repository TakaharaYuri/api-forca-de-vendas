'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeminovosSchema extends Schema {
  up () {
    this.create('seminovos', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('seminovos')
  }
}

module.exports = SeminovosSchema
