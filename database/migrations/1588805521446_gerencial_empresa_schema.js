'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresaSchema extends Schema {
  up () {
    this.create('empresas', (table) => {
      table.string('nome', 240).notNullable()
      table.string('razao_social')
      table.string('cnpj')
      table.string('cep')
      table.string('estado')
      table.string('cidade')
      table.string('rua')
      table.string('numero')
      table.string('complemento')
      table.string('logo')
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('empresas')
  }
}

module.exports = EmpresaSchema
