"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UsuarioDadosSchema extends Schema {
  up() {
    this.alter("usuario_dados", (table) => {
      table.string("bio");
      table.datetime("data_admissao");
    });
  }

  down() {
    this.drop("usuario_dados");
  }
}

module.exports = UsuarioDadosSchema;
